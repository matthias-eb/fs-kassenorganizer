FROM archlinux:latest

# Arguments
ARG USER_ID
ARG USER_PW
ARG GROUP_ID
ARG ROOT_PW

RUN echo "root:${ROOT_PW}" | chpasswd
RUN groupadd --gid ${GROUP_ID} fachschaft
RUN useradd -m -s /bin/bash --uid ${USER_ID} --gid ${GROUP_ID} fachschaft
RUN echo "fachschaft:${USER_PW}" | chpasswd

# Install java, pdftk and go
COPY mcpdf.jar /home/fachschaft/jar/
# glibc is a dependency of Java. Pdfinject uses java and the mcpdf jar file to create a filled pdf form.
RUN pacman --noconfirm -Sy jre8-openjdk java-commons-lang glibc git fakeroot
# Install wkhtmltopdf
RUN pacman --noconfirm -S fontconfig freetype2 graphite harfbuzz libjpeg6-turbo libpng libxrender
RUN pacman -S --noconfirm ttf-dejavu gnu-free-fonts ttf-liberation ttf-ubuntu-font-family
WORKDIR /home/fachschaft

RUN git clone "https://aur.archlinux.org/wkhtmltopdf-static.git"
RUN chown fachschaft ./wkhtmltopdf-static
RUN runuser -l fachschaft -c "cd /home/fachschaft/wkhtmltopdf-static && makepkg -sc --noconfirm"
RUN pacman -U --noconfirm /home/fachschaft/wkhtmltopdf-static/wkhtmltopdf-static-0.12.6-2-x86_64.pkg.tar.zst
# Install go and pdftk
RUN pacman --noconfirm -S go pdftk


WORKDIR /go/src/gitlab.com/matthias-eb/fs-kassenorganizer
COPY fs-kassenorganizer.go .
COPY interfaces ./interfaces
COPY initialize ./initialize
COPY templates ./templates
COPY constants ./constants
COPY model ./model
COPY util ./util
COPY runners ./runners
COPY html ./html
COPY routes ./routes
COPY runners ./runners
# This RUN true command is to mitigate a bug existing in docker, where the docker build fails if the diff is 0 after multiple COPY commands. 
RUN true
COPY Kassenanordnung_2020_2021.pdf .
COPY docker.env ./local.env
COPY go.mod .
COPY go.sum .
RUN go build

# Install pdfcpu binary
WORKDIR /root/go/pkg/mod/github.com/pdfcpu/pdfcpu@v0.3.13/cmd/pdfcpu
RUN go install
RUN ln /root/go/bin/pdfcpu /usr/bin/pdfcpu

RUN mkdir /home/fachschaft/fs-kassenorganizer
RUN chown -R fachschaft:fachschaft /go /home/fachschaft/fs-kassenorganizer /home/fachschaft/jar /home/fachschaft/wkhtmltopdf-static

WORKDIR /go/src/gitlab.com/matthias-eb/fs-kassenorganizer

EXPOSE 8080


CMD ["./fs-kassenorganizer"]