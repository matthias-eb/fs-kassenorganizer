package interfaces

import (
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
	ntw "moul.io/number-to-words"
)

type Einnahme_Ausgabe_Rb string

const (
	Einnahme Einnahme_Ausgabe_Rb = "Einnahme"
	Ausgabe  Einnahme_Ausgabe_Rb = "Ausgabe"
)

type Kassenanweisung struct {
	Einnahme_Ausgabe_rb          Einnahme_Ausgabe_Rb
	Haushaltsjahr                string
	Titelnr                      int
	Betrag                       float64
	Betrag_In_Worten             string
	Einzahler_Empfänger_1        string
	Einzahler_Empfänger_2        string
	Kreditinstitut_Auszahlung    string
	IBAN_Auszahlung              string
	BIC_Auszahlung               string
	Begründung_1                 string
	Begründung_2                 string
	Rechnung_cb                  bool
	Überweisungsbeleg_cb         bool
	Quittung_cb                  bool
	Ort_Datum_AStAMitglied       string
	Ort_Datum_Finanzreferentin   string
	Buchführungstitel            string
	Buchführungsdatum            string
	Gebucht_am                   string
	Buchführung_Bemerkungen      string
	Buchführung_Bemerkungen_2    string
	Ort_Datum_Buchhalterin       string
	Kassenverwaltung_am          string
	Kassenverwaltung_IBAN_cb     bool
	Kassenverwaltung_IBAN        string
	Kassenverwaltung_AuszugNr    string
	Sonstiges_Konto_cb           bool
	Sonstiges_Konto_Kontonr      string
	Sonstiges_Konto_AuszugNr     string
	Kassenverwaltung_Barkasse_cb bool
	QuittungsNr                  string
	Kassenverwaltung_Bemerkung_1 string
	Kassenverwaltung_Bemerkung_2 string
	Ort_Datum_Kassenverwaltung   string
}

func (ks *Kassenanweisung) FillBetragInWorten() {
	p := message.NewPrinter(language.German)
	betrag_string := p.Sprintf("%.2f", ks.Betrag)
	betrag_string = strings.ReplaceAll(betrag_string, ".", "")
	spl := strings.Split(betrag_string, ",")
	vor_Komma, err := strconv.ParseInt(spl[0], 10, 64)
	if err != nil {
		fmt.Println(err.Error())
		ks.Betrag_In_Worten = ""
	}
	nach_Komma, err := strconv.ParseInt(spl[1], 10, 64)
	if err != nil {
		fmt.Println(err.Error())
		ks.Betrag_In_Worten = ""
	}
	if nach_Komma == 0 {
		ks.Betrag_In_Worten = ntw.IntegerToDeDe(int(vor_Komma))
	} else {
		ks.Betrag_In_Worten = (ntw.IntegerToDeDe(int(vor_Komma)) + " " + ntw.IntegerToDeDe(int(nach_Komma)))
	}
}
func (ks Kassenanweisung) CBValue(b bool) string {
	if b {
		return "On"
	}
	return "Off"
}
