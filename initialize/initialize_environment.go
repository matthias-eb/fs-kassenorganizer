package initialize

import env "github.com/joho/godotenv"

func init() {
	env.Load("local.env")
}

// This is function that ensures that the init() method is called.
func Initialize() bool {
	return true
}
