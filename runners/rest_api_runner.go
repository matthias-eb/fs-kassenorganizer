package runners

import (
	"fmt"

	"github.com/gin-gonic/gin"
	rts "gitlab.com/matthias-eb/fs-kassenorganizer/routes"
)



func StartRestAPI() {
	router := gin.Default()

	// Load templates
	router.LoadHTMLGlob("templates/*")
	//register routes
	rts.RegisterKassenanweisungHandlers(router.Group("/kassenanweisungen"))
	rts.RegisterKassenpruefungenHandlers(router.Group("/kassenpruefungen"))
	rts.RegisterAnlageortHandlers(router.Group("/geldanlagen"))
	rts.RegisterJahresabschlussRoutes(router.Group("/jahresabschluss"))
	router.StaticFile("/favicon.ico", "assets/favicon.ico")

	fmt.Println("Server runs on Port 8080")
	router.Run(":8080")
}
