package runners

import (
	"encoding/json"
	"fmt"
	"strconv"

	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	mdl "gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/jahresabschlussrechner"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/pdf_filler"
)

func ConsoleMenu() {
	fmt.Println("Willkommen beim Fachschaftsorganizer.")
	pos := util.SingleChoice("Hauptmenü", "Daten in die Konsole ausgeben", "Daten in PDF Dateien schreiben", "Jahresabschluss berechnen", "Programm beenden")
	switch pos {
	case 1, 2:
		pos2 := util.SingleChoice("Datenmenge auswählen", "Alle Daten", "Für Haushaltsjahr", "Für Haushaltsjahr ab Titelnummer", "Für Haushaltsjahr bestimmte Titelnummern")
		var entries []mdl.Kassenanweisung_model = []mdl.Kassenanweisung_model{}
		switch pos2 {
		case 1:
			entries = mdl.GetAllKassenanweisungen()
		case 2:
			haushaltsjahre := mdl.GetHaushaltsjahre()
			hj_ind := util.SingleChoice("Haushaltsjahr wählen", haushaltsjahre...)
			entries = mdl.QueryKassenanweisungForHaushaltsjahr(haushaltsjahre[hj_ind-1])
		case 3:
			haushaltsjahre := mdl.GetHaushaltsjahre()
			hj_ind := util.SingleChoice("Haushaltsjahr wählen", haushaltsjahre...)
			titelnummern := mdl.GetTitelnummernForHaushaltsjahr(haushaltsjahre[hj_ind-1])
			titelnr_string := []string{}
			for i := range titelnummern {
				titelnr_string = append(titelnr_string, strconv.FormatInt(int64(titelnummern[i]), 10))
			}
			tn_ind := util.SingleChoice("Titelnummer wählen", titelnr_string...)
			entries = mdl.QueryKassenanweisungAfterTitleNrForHaushaltsjahr(haushaltsjahre[hj_ind-1], titelnummern[tn_ind-1]-1)
		case 4:
			haushaltsjahre := mdl.GetHaushaltsjahre()
			hj_ind := util.SingleChoice("Haushaltsjahr wählen", haushaltsjahre...)
			titelnummern := mdl.GetTitelnummernForHaushaltsjahr(haushaltsjahre[hj_ind-1])
			titelnr_string := []string{}
			for i := range titelnummern {
				titelnr_string = append(titelnr_string, strconv.FormatInt(int64(titelnummern[i]), 10))
			}
			tn_ind := util.MultipleChoice("Titelnummer wählen", titelnr_string...)
			titelnr_string = []string{}
			fmt.Print("Nummern: ")
			for _, tn := range tn_ind {
				titelnr_string = append(titelnr_string, strconv.FormatInt(int64(titelnummern[tn-1]), 10))
				fmt.Printf("%+v ", tn)
			}
			fmt.Println()
			entries = mdl.QueryKassenanweisungTitleNrForHaushaltsjahr(haushaltsjahre[hj_ind-1], titelnr_string)
		default:
			break
		}
		if pos == 1 {
			printKassenanweisungen(entries)
		} else if pos == 2 {
			pdf_filler.KassenanweisungenToPDF(entries)
		}
	case 3:
		haushaltsjahre := model.GetHaushaltsjahre()
		wahl := util.SingleChoice("Haushaltsjahr wählen", haushaltsjahre...)
		jahresabschluesse := jahresabschlussrechner.DoJahresabschluss(haushaltsjahre[wahl-1])
		bytes, err := json.MarshalIndent(jahresabschluesse, "||", "\t")
		if err != nil {
			fmt.Printf("Error while displaying jahresabschluesse: %+v", err)
			return
		}

		fmt.Printf("Jahresabschluesse:\n%+v", bytes)
	default:
		return
	}
}

func printKassenanweisungen(kassenanweisungen []mdl.Kassenanweisung_model) {
	for _, ks := range kassenanweisungen {
		fmt.Println(ks.Stringify())
	}
}
