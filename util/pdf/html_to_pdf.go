package pdf

import (
	"fmt"
	"io"
	"os"

	html2pdf "github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
)

func SaveHtmlAsPDF(htmlPath string, pdfName string) error {
	f, err := os.OpenFile(constants.REL_ROOT+"html_to_pdf.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Printf("Error opening file: %v", err)
	}
	defer f.Close()
	mwriter := io.MultiWriter(f, os.Stdout)
	pdfg, err := html2pdf.NewPDFGenerator()
	if err != nil {
		return err
	}

	// Set global options
	pdfg.Dpi.Set(300)
	pdfg.Orientation.Set(html2pdf.OrientationLandscape)

	page := html2pdf.NewPage(htmlPath)
	page.Encoding.Set("utf-8")

	pdfg.AddPage(page)
	pdfg.SetStderr(mwriter)

	// Create PDF document in internal buffer
	err = pdfg.Create()
	if err != nil {
		return err
	}

	// Write buffer contents to file on disk
	err = pdfg.WriteFile(constants.PDF_JAHRESABSCHL_FOLDER + pdfName)
	if err != nil {
		return err
	}
	return nil
}
