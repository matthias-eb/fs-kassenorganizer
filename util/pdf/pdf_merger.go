package pdf

import (
	"errors"
	"fmt"
	"os"

	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	cst "gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	mdl "gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/pdf_filler"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/util_json"
)

func CreateAndMergePDFs(kassenanweisungen []mdl.Kassenanweisung_model, filename string) (string, error) {
	var filename_json = fmt.Sprintf("%s.json", filename)
	var filename_pdf = fmt.Sprintf("%s.pdf", filename)
	// Check the existence of the pdf and json file
	_, err1 := os.Stat(cst.JSON_MERGE_FOLDER + filename_json)
	_, err2 := os.Stat(cst.PDF_MERGE_FOLDER + filename_pdf)

	// Create the files if necessary
	if os.IsNotExist(err1) || os.IsNotExist(err2) {
		if os.IsNotExist(err2) {
			util.CreateDirIfNotExists(cst.PDF_MERGE_FOLDER)
		} else {
			util.CreateDirIfNotExists(cst.JSON_MERGE_FOLDER)
		}

		file_paths := pdf_filler.KassenanweisungenToPDF(kassenanweisungen)
		err := CreateMergedPDF(file_paths, kassenanweisungen, filename_pdf, filename_json)
		if err != nil {
			fmt.Println(err)
			return "", err
		}

	} else {
		err := CheckAndFixPDF(kassenanweisungen, filename_pdf, filename_json)
		if err != nil {
			if err.Error() == constants.ERR_NO_CHANGES {
				// Report that there were no changes
				return cst.PDF_MERGE_FOLDER + filename_pdf, err
			}
			fmt.Println(err)
			return "", err
		}
	}
	return cst.PDF_MERGE_FOLDER + filename_pdf, nil
}

func CreateMergedPDF(filepaths []string, kassenanweisungen []mdl.Kassenanweisung_model, pdf_name string, json_name string) error {

	err := pdf_filler.MergePDFs(cst.PDF_MERGE_FOLDER, pdf_name, filepaths)
	if err != nil {
		fmt.Println(err)
		return err
	}
	// Create Json File
	util_json.CreateJsonRepresentation(kassenanweisungen, cst.JSON_MERGE_FOLDER+json_name)
	return nil
}

// This method checks if the PDF contains the Kassenanweisungen given or if the content of the json file differs from this.
// If it differs, the PDF file is rewritten together with the json File.
func CheckAndFixPDF(kassenanweisungen []mdl.Kassenanweisung_model, pdf_file string, json_file string) error {
	differs, err := util_json.JsonDiffers(cst.JSON_MERGE_FOLDER+json_file, kassenanweisungen)
	if err != nil {
		return err
	}
	if differs {
		file_paths := pdf_filler.KassenanweisungenToPDF(kassenanweisungen)
		err := CreateMergedPDF(file_paths, kassenanweisungen, cst.PDF_MERGE_FOLDER+pdf_file, json_file)
		if err != nil {
			return err
		}
	} else {
		fmt.Println("No changes in the cass orders, returning.")
		return errors.New(constants.ERR_NO_CHANGES)
	}
	return nil
}
