package util

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func Contains(strs []string, str ...string) bool {
	for _, s := range strs {
		for _, s2 := range str {
			if s == s2 {
				return true
			}
		}
	}
	return false
}

func ComputeHashForIntList(list []int, delim string) string {
	var buffer bytes.Buffer
	for i := range list {
		buffer.WriteString(strconv.Itoa(list[i]))
		buffer.WriteString(delim)
	}
	return fmt.Sprintf("%v", (sha256.Sum256(buffer.Bytes())))
}

func SplitAtMaxBreakpointWord(s string, maxChars int) (string, string) {
	last_space := 0
	for i, c := range s {
		if c == ' ' {
			last_space = i
		}
		if i == maxChars {
			if last_space != 0 {
				var end []string = make([]string, len(s)-last_space)
				var front []string = make([]string, last_space)
				end = append(end, s[last_space+1:])
				front = append(front, s[:last_space])
				return strings.Join(front, ""), strings.Join(end, "")
			} else {
				var end []string = make([]string, len(s)-maxChars)
				var front []string = make([]string, maxChars)
				end = append(end, s[maxChars+1:])
				front = append(front, s[:maxChars])
				return strings.Join(front, ""), strings.Join(end, "")
			}
		}
	}
	return s, ""
}

func SingleChoice(title string, options ...string) int {
	i := -1
	reader := bufio.NewScanner(os.Stdin)
	for i < 0 {
		fmt.Printf("##%+v##\n", title)
		for j := 0; j < len(options); j++ {
			fmt.Printf("%+v) %+v\n", j+1, options[j])
		}
		fmt.Printf("Choose an option: (%+v - %+v):", 1, len(options))
		reader.Scan()
		answer := reader.Text()
		answer_int, err := strconv.ParseInt(answer, 10, 8)
		if err != nil {
			continue
		}
		if answer_int > 0 && int(answer_int) <= len(options) {
			i = int(answer_int)
		}
	}
	return i
}

func MultipleChoice(title string, options ...string) []int {
	choices := []int{}
	i := -1
	reader := bufio.NewScanner(os.Stdin)
	for i < 0 {
		fmt.Printf("##%+v##\n", title)
		for j := 0; j < len(options); j++ {
			fmt.Printf("%+v) %+v\n", j+1, options[j])
		}
		fmt.Printf("Choose one or more options: (%+v - %+v, %+v):", 1, 4, 15)
		reader.Scan()
		answer := reader.Text()
		answerparts := strings.Split(answer, ",")
		for _, ap := range answerparts {
			ap = strings.Trim(ap, " ")
			answer_int, err := strconv.ParseInt(ap, 10, 16)
			if err != nil {
				dash_split := strings.Split(ap, "-")
				if len(dash_split) != 2 {
					break
				}
				border_left := strings.Trim(dash_split[0], " ")
				border_right := strings.Trim(dash_split[1], " ")
				bl_int, err := strconv.ParseInt(border_left, 10, 16)
				if err != nil {
					break
				}
				br_int, err := strconv.ParseInt(border_right, 10, 16)
				if err != nil {
					break
				}
				if bl_int > br_int {
					h := bl_int
					bl_int = br_int
					br_int = h
				}
				for a := bl_int; a <= br_int && int(a) <= len(options) && a >= 1; a++ {
					if !containsInt(choices, int(a)) {
						choices = append(choices, int(a))
						i = int(a)
					}
				}
				continue
			}
			if answer_int > 0 && int(answer_int) <= len(options) {
				if !containsInt(choices, int(answer_int)) {
					i = int(answer_int)
					choices = append(choices, i)
				}
			}
		}
	}
	return choices
}

func containsInt(ints []int, i int) bool {
	for _, val := range ints {
		if val == i {
			return true
		}
	}
	return false
}

func CreateDirIfNotExists(path string) {
	dir := filepath.Dir(path)

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(path, 0777)
		if err != nil {
			fmt.Println(err)
			panic(err)
		}
	}
}

func CheckRegularFile(path string) error {
	sourceFileStat, err := os.Stat(path)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", path)
	}
	return nil
}
