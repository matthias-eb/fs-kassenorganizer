package util_json

import (
	"encoding/json"
	"os"

	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/types"
)

func CreateJSONFileJahresabschluss(jahresabschluss types.Jahresabschluss_Geldanlage, json_path string) {
	b, err := json.Marshal(jahresabschluss)
	if err != nil {
		panic(err)
	}
	writeJSONFile(b, json_path)
}

func CreateJsonRepresentationKw(kassenanweisung model.Kassenanweisung_model, json_path string) {
	b, err := json.Marshal(kassenanweisung)
	if err != nil {
		panic(err)
	}
	writeJSONFile(b, json_path)
}

func CreateJsonRepresentation(kassenanweisungen []model.Kassenanweisung_model, json_path string) {
	b, err := json.Marshal(kassenanweisungen)
	if err != nil {
		panic(err)
	}
	writeJSONFile(b, json_path)
}

func writeJSONFile(bytestream []byte, json_path string) {
	util.CreateDirIfNotExists(json_path)
	f, err := os.OpenFile(json_path, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	_, err = f.Write(bytestream)
	if err != nil {
		panic(err)
	}
}

func JsonDiffersKw(json_path string, kassenanweisung model.Kassenanweisung_model) bool {
	util.CreateDirIfNotExists(json_path)
	json_bytes, err := os.ReadFile(json_path)
	if err != nil {
		if os.IsNotExist(err) {
			CreateJsonRepresentationKw(kassenanweisung, json_path)
			return true
		}
		panic(err)
	}

	marshal_bytes, err := json.Marshal(kassenanweisung)
	if err != nil {
		panic(err)
	}

	// Compare the kassenanweisungen Json with the one from the Json File.
	// If the content is equal, serve the Zip file immediately, otherwise, add only the missing content
	if string(marshal_bytes) == string(json_bytes) {
		return false
	}
	return true
}

func JsonDiffers(json_path string, kassenanweisungen []model.Kassenanweisung_model) (bool, error) {
	util.CreateDirIfNotExists(json_path)
	json_bytes, err := os.ReadFile(json_path)
	if err != nil {
		if os.IsNotExist(err) {
			CreateJsonRepresentation(kassenanweisungen, json_path)
			return true, nil
		}
		return true, err
	}

	marshal_bytes, err := json.Marshal(kassenanweisungen)
	if err != nil {
		return true, err
	}

	// Compare the kassenanweisungen Json with the one from the Json File.
	// If the content is equal, serve the Zip file immediately, otherwise, add only the missing content
	if string(marshal_bytes) == string(json_bytes) {
		return false, nil
	}

	// Content is not equal, fill in kassenanweisung array to compare each one
	kassenanweisungen_json := []model.Kassenanweisung_model{}
	err = json.Unmarshal(json_bytes, &kassenanweisungen_json)
	if err != nil {
		return true, err
	}

	if len(kassenanweisungen_json) != len(kassenanweisungen) {
		return true, nil
	}

	for i, ka := range kassenanweisungen {
		if !ka.Equals(kassenanweisungen_json[i]) {
			return true, nil
		}
	}
	return false, nil
}
