package jahresabschluss

import (
	"archive/zip"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/pdf_filler"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/util_zip"
)

func GetOrCreateJahresabschlussZIP(hhj string, id int64) (string, error) {
	if len(hhj) != 5 {
		return "", errors.New("wrong or unknown formatting for haushaltsjahr. Format like this: 'YY/YY'")
	}
	n1 := hhj[0:2]
	n2 := hhj[3:5]

	t1 := time.Now()
	fileName, err := GetJahresabschlussPDF(id, hhj)
	if err != nil {
		if err.Error() == constants.ERR_NO_CHANGES {
		} else {
			panic("Cannot create Jahresabschluss Table PDF. This shouldn't happen unless the program isn't installed properly.")
		}
	}

	kassenanweisungen := model.QueryKassenanweisungenForHaushaltsjahrForGeldanlageOrdered(n1+"/"+n2, int(id), "Titelnr ASC")

	filename_Kassenanweisungen := fmt.Sprintf("%s%d-%s", constants.ID_HHJ_NAME_PREFIX, id, n1+"_"+n2)

	t2 := time.Now()

	filename_all_zip, err := GetJahresabschlussZIP(filename_Kassenanweisungen, kassenanweisungen, fileName)
	if err != nil {
		panic(err)
	}
	d := time.Since(t1)
	d1 := time.Since(t2)
	fmt.Println("Time needed for generating a complete ZIP file: ", d1.Seconds(), "s, elapsed time since t1: ", d.Seconds(), "s")
	return filename_all_zip, nil
}

func GetJahresabschlussZIP(filename_Kassenanweisungen string, kassenanweisungen []model.Kassenanweisung_model, filename_Jahresabschluss_PDF string) (string, error) {
	filename_json := filename_Kassenanweisungen + ".json"
	filename_zip := filename_Kassenanweisungen + ".zip"

	// Check if the ZIP file already exists, and if so, if the JSON exists as well
	_, err1 := os.Stat(constants.JSON_FOLDER + filename_json)
	_, err2 := os.Stat(constants.ZIP_FOLDER + filename_zip)

	// Create ZIP and send it back
	if os.IsNotExist(err1) || os.IsNotExist(err2) {
		if os.IsNotExist(err2) {
			util.CreateDirIfNotExists(constants.ZIP_FOLDER)
		} else {
			util.CreateDirIfNotExists(constants.JSON_FOLDER)
		}

		file_paths := pdf_filler.KassenanweisungenToPDF(kassenanweisungen)
		util_zip.CreateZipAndJsonFile(file_paths, kassenanweisungen, filename_zip, filename_json)

	} else {
		util_zip.AddMissingKassenanweisungenToZip(kassenanweisungen, filename_zip, filename_json)
	}

	// Copy the created Zip File to the final position where the overview pdf file will be added
	filename_all_zip := filename_Kassenanweisungen + "_all.zip"
	util.CheckRegularFile(constants.ZIP_FOLDER + filename_zip)
	source, err := os.Open(constants.ZIP_FOLDER + filename_zip)
	if err != nil {
		return "", err
	}
	defer source.Close()

	dest, err := os.Create(constants.ZIP_FOLDER + filename_all_zip)
	if err != nil {
		return "", err
	}
	defer dest.Close()
	_, err = io.Copy(dest, source)
	if err != nil {
		return "", err
	}
	dest.Sync()

	zipWriter := zip.NewWriter(dest)
	defer zipWriter.Close()
	// Place the overview in the root of the zip file
	err = util_zip.AddFileToZip(zipWriter, constants.PDF_JAHRESABSCHL_FOLDER+filename_Jahresabschluss_PDF, filepath.Dir(constants.PDF_JAHRESABSCHL_FOLDER))
	if err != nil {
		return "", err
	}
	zipWriter.Flush()
	zipWriter.Close()
	return filename_all_zip, nil
}
