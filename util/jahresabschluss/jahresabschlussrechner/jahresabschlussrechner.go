package jahresabschlussrechner

import (
	"errors"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/types"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func DoJahresabschluss(haushaltsjahr string) (jahresabschluesse []types.Jahresabschluss_Geldanlage) {

	/*ToDo:
	Alle Geldanlagen holen und für jede davon die folgenden Punkte machen:

	1.: Anfangssaldo aus der letzten Kassenprüfung berechnen
	2.: Endsaldo berechnen
	3.: Endsaldo und Anfangssaldo mit Kassenprüfungen überprüfen. Wenn es nicht stimmt, muss der Benutzer die Datenbank von Hand ändern
	4.: Kassenanweisungen in Einnahmenseite und Ausgabenseite unterteilen. Dabei ist vor allem auf Geldtransfers und Einzahlungen zu achten,
	bei denen anhand des Empfängers oder des Geldgebers ermittelt werden muss, ob das Geld dazu kommt oder wegfällt.
	5.: Summe an Ausgaben berechnen
	6.: Summe an Einnahmen berechnen
	7.: Werte in HTML Seite eintragen und Tabellarisch auflisten, nach Datum sortiert.
	8.: in PDF Datei drucken Dabei darauf achten, dass die Seite im Querformat bedruckt werden muss, damit die Seite gut ausgenutzt wird.
	*/

	/*	entriesHJ := GetEntriesForHaushaltsjahr(haushaltsjahr)

		// starting balance for each of the banks (maybe from the database?)
		reader := bufio.NewScanner(os.Stdin)
	*/

	inhaber := model.GetInhaberByName(os.Getenv("INHABER_NAME"))

	geldanlagen := model.GetGeldanlagenByInhaber(int(inhaber.Id))

	for _, geldanlage := range geldanlagen {
		jahresabschluss := GetJahresabschlussForHaushaltsjahrForAnlageId(haushaltsjahr, geldanlage.Id)
		jahresabschluesse = append(jahresabschluesse, jahresabschluss)
	}

	return
}

func GetJahresabschlussForHaushaltsjahrForAnlageId(haushaltsjahr string, anlageId int) types.Jahresabschluss_Geldanlage {
	fehlermeldungen := []string{}
	fehlerwert := types.Korrekt
	geldanlage, err := model.GetGeldanlage(anlageId)
	lang := language.MustParse(os.Getenv("APP_LANG"))
	p := message.NewPrinter(lang)
	cur, _ := currency.FromTag(lang)
	curSymbol := currency.Symbol(cur)

	if err != nil {
		panic(err)
	}

	//Kassenprüfungen für Haushaltsjahr holen. Kassenprüfungen sind nach Datum aufsteigend sortiert.
	kassenpruefungen, err := model.GetKassenpruefungenForAnlageortInHaushaltsjahr(anlageId, haushaltsjahr)
	if err != nil {
		panic(err)
	}

	// Kassenanweisungen für Haushaltsjahr holen. Kassenanweisungen sind nach Zahlungsdatum aufsteigend sortiert.
	kassenanweisungen := model.QueryKassenanweisungenForHaushaltsjahrForGeldanlageOrdered(haushaltsjahr, anlageId, "Zahlungsdatum ASC")

	if len(kassenpruefungen) == 0 {
		fehlermeldungen = append(fehlermeldungen, "Es gibt keine Kassenprüfung in diesem Haushaltsjahr, von der aus der Jahresendbetrag oder Anfangsbetrag berechnet werden könnte")
		fehlerwert = types.Fehler
	}

	kassenstaende := []types.Kassenstand{}

	// Anfangsbetrag und Endbetrag sowie Einnahmen und Ausgaben errechnen und für jede Kassenprüfung die Start- und Endsaldi überprüfen
	var saldo_start, saldo_end, einnahmen, ausgaben float64
	einnahmen, ausgaben = 0, 0
	kp_cnt := 0
	var kp model.Kassenpruefung
	var was_inside bool
	var kass_fuer_datum []model.Kassenanweisung_model // Kassenanweisungen, die an einem Tag liegen
	current_date := time.Time{}
	kassenstand_tagesbeginn, kassenstand_tagesende := 0.00, 0.00
	n := 1 // Zählt alle Kassenanweisungen für einen Tag hoch
	x := 0 // Zählt alle Kassenprüfungen für den Tag der Kassenanweisungen und danach

	//Array mit Beträgen erstellen, der berücksichtigt, ob die Zahlung eine Einnahme oder Ausgabe ist.
	//kas_betraege beinhaltet die Beträge der Kassenanweisungen als float array
	kas_betraege := getGeldSummanden(kassenanweisungen, anlageId)

	// Gehe alle Kassenanweisungen durch. Überspringe die Kassenanweisungen für denselben Tag (n)
	for i := 0; i < len(kassenanweisungen); i += n {

		ka := kassenanweisungen[i]
		if current_date == (time.Time{}) {
			// Erste Ausführung der Schleife
			current_date = ka.Zahlungsdatum

			//Berechne ersten Tagesstartsaldo. Falls keine Kassenprüfung existiert, setze ihn auf 0.
			if len(kassenpruefungen) > 0 {
				kp0 := kassenpruefungen[0]
				kassenstand_tagesbeginn = kp0.Betrag
				for index := 0; index < len(kassenanweisungen); index++ {
					if kassenanweisungen[index].Zahlungsdatum.After(kp0.Datum) {
						break
					} else {
						kassenstand_tagesbeginn -= kas_betraege[index]
					}
				}
				saldo_start = kassenstand_tagesbeginn

				// Erhöhe den kassenpruefung Counter, um diese als Startwert definierte Kassenprüfung zu überspringen
				kp = kp0
				kp_cnt++
			} else {
				kassenstand_tagesbeginn = 0
				saldo_start = 0
			}
		} else {
			current_date = ka.Zahlungsdatum
		}

		// Finde die Anzahl an Kassenanweisungen mit dem gleichen Datum und füge sie dem Array kass_fuer_datum hinzu
		kass_fuer_datum = []model.Kassenanweisung_model{}
		kass_fuer_datum = append(kass_fuer_datum, kassenanweisungen[i]) // Erste Kassenanweisung eines Datums gehört sowieso zum aktuellen Datum
		kass_fuer_datum_betraege := []float64{kas_betraege[i]}          // Beinhaltet die Beträge für ein Datum

		// Füge Einnahmen den Einnahmen hinzu und Ausgaben den Ausgaben
		if kas_betraege[i] < 0 {
			ausgaben -= kas_betraege[i]
		} else {
			einnahmen += kas_betraege[i]
		}

		// Verhindere, über die Array Länge hinaus zu laufen
		if i != len(kassenanweisungen)-1 {
			// Sammle Kassenanweisungen für den Tag
			for n = 1; kassenanweisungen[i+n].Zahlungsdatum.Equal(current_date); n++ {
				kass_fuer_datum = append(kass_fuer_datum, kassenanweisungen[i+n])
				kass_fuer_datum_betraege = append(kass_fuer_datum_betraege, kas_betraege[i+n])

				// Füge Einnahmen den Einnahmen hinzu und Ausgaben den Ausgaben
				if kas_betraege[i+n] < 0 {
					ausgaben -= kas_betraege[i+n]
				} else {
					einnahmen += kas_betraege[i+n]
				}

				// Verhindere, über die Array Länge hinaus zu laufen
				if (i + n + 1) == len(kassenanweisungen) {
					n++
					break
				}
			}
		}

		kassenpruefungen_Zeitraum := []model.Kassenpruefung{}
		was_inside = false

		// Kassenprüfungen für dieses Datum finden
		// kp_cnt ist ein Index für die Kassenprüfungen. Er stellt sicher, dass die Kassenprüfungen dort weiter geprüft werden,
		// wo bei dem letzten Satz Kassenanweisungen aufgehört wurde. X ist der Zähler für die Kassenprüfungen, die innerhalb dieser Kassenanweisung liegen.
		if len(kassenpruefungen) > kp_cnt {
			jump_kp_cnt := 0
			// Finde alle Kassenprüfungen, die vor der nächsten Kassenanweisung und nach oder am selben Tag wie das aktuelle Datum liegen
			for x = 0; x+kp_cnt < len(kassenpruefungen); x++ {
				kp = kassenpruefungen[x+kp_cnt]
				was_inside = true
				if kp.Datum.Before(current_date) {
					jump_kp_cnt++
					if kassenstand_tagesbeginn != 0 && kassenstand_tagesbeginn != kp.Betrag {
						fehlermeldungen = append(fehlermeldungen, p.Sprintf("Fehler: Kassenstand zu Tagesbeginn ist nicht 0 aber der Kassenstand von %.2f %v ist nicht gleich dem Betrag der Kassenprüfung mit %.2f %v.", kassenstand_tagesbeginn, curSymbol, kp.Betrag, curSymbol))
					}
					// Wenn jump_kp_cnt == 2, dann ist jetzt das 2. Mal, dass eine Kassenprüfung übersprungen wurde. wenn sich die Beträge nicht gleichen, muss hier eine Kassenanweisung fehlen.
					if jump_kp_cnt == 2 && kp.Betrag != kassenpruefungen[x+kp_cnt-1].Betrag {
						fehlermeldungen = append(fehlermeldungen, p.Sprintf("Fehler: Zwischen dem %v und dem %v hat sich der Betrag geändert (von %.2f %v auf %.2f %v) aber es gibt keine Kassenanweisung dazu.", kassenpruefungen[x+kp_cnt-1].Datum, kp.Datum, kassenpruefungen[x+kp_cnt-1].Betrag, curSymbol, kp.Betrag, curSymbol))
					}
					kassenstand_tagesbeginn = kp.Betrag

				} else if kp.Datum.Equal(current_date) {
					kassenpruefungen_Zeitraum = append(kassenpruefungen_Zeitraum, kp)

				} else if kp.Datum.After(current_date) {
					if (i + n) < len(kassenanweisungen) {
						if !kp.Datum.Before(kassenanweisungen[i+n].Zahlungsdatum) {
							// Kassenprüfung gehört zur nächsten Kassenanweisung, da das Datum der Kassenprüfung hinter oder auf dem der nächsten Kassenanweisung liegt.
							break
						}
					}

					// Kassenprüfungsdatum is nach dem aktuellen Datum aber vor dem kommenden Kassenanweisungsdatum - Kassenstand des Tagesendes

					kassenpruefungen_Zeitraum = append(kassenpruefungen_Zeitraum, kp)

					// Überprüfe, ob mehrere Kassenprüfungen im selben Zeitraum mit unterschiedlichen Werten existieren. Wenn ja, fehlt eine Kassenanweisung.
					if x > 0 && math.Round(kassenstand_tagesende*100)/100 != math.Round(kp.Betrag*100)/100 {
						var date_end string
						if len(kassenanweisungen) <= i+n {
							hhj_split := strings.Split(haushaltsjahr, "/")
							hhj_end, _ := strconv.ParseInt("20"+hhj_split[1], 10, 32)
							date_end = time.Date(int(hhj_end), 3, 1, 0, 0, 0, 0, time.Local).Format("02.01.2006")
						} else {
							date_end = kassenanweisungen[i+n].Zahlungsdatum.Format("02.01.2006")
						}
						fehlermeldungen = append(fehlermeldungen, p.Sprintf("Eine Kassenanweisung zwischen dem %v und dem %v fehlt: Der Tagesendstand laut Kassenprüfung vom %v von %.2f€ entspricht nicht dem neuen Tagesendbetrag im selben Zeitraum von %.2f€", ka.Zahlungsdatum.Format("02.01.2006"), date_end, kp.Datum, math.Round(kp.Betrag*100)/100, math.Round(kassenstand_tagesende*100)/100))
					}
					kassenstand_tagesende = math.Round(kp.Betrag*100) / 100
					fehlerwert = types.Bestätigt
				}
			}
			if was_inside {
				kp_cnt += x
			}
		}
		// Überprüfe die Kassenprüfungen des aktuellen Datums, sie müssen einen Betrag beinhalten, der durch die kassenanweisungen verursacht werden kann
		// Überprüfe, ob start- und endbetrag des Tages ermittelt wurden. Wenn nicht, füge sie hinzu. Ist das nicht möglich, panic.
		// Tagesendbetrag errechnen
		sum_day_end := kassenstand_tagesbeginn
		for _, betr := range kass_fuer_datum_betraege {
			sum_day_end += betr
		}

		// Überprüfe, ob die durch die Kassenprüfungen berechneten Werte mit denen durch die Kassenanweisungen berechneten Werte überein stimmen, allerdings nur, wenn es eine Kassenprüfung in diesem Zeitrum gab (durch was_inside angezeigt)
		if was_inside && math.Round(sum_day_end*100)/100 != math.Round(kassenstand_tagesende*100)/100 && kassenstand_tagesende != 0 {
			fehlermeldungen = append(fehlermeldungen, fmt.Sprintf("Ein durch eine Kassenprüfung bestätigter Kassenstand am Tagesende des %+v stimmt nicht mit den Einnahmen und Ausgaben überein. Möglicherweise fehlt eine Kassenanweisung.", current_date.Format("02.01.2006")))
			fehlerwert = types.Fehler
		} else if kassenstand_tagesende == 0 || !was_inside {
			kassenstand_tagesende = sum_day_end
		}

		// Berechne alle möglichen Saldi
		possible_values := CalculatePossibleSaldoValues(kassenstand_tagesbeginn, kass_fuer_datum_betraege, kassenstand_tagesende)

		sort.Slice(possible_values, func(i2, j int) bool {
			return i2 < j
		})

		// Gehe die Liste von Kassenprüfungen am Tag durch und überprüfe, ob der Betrag mit einem der Beträge übereinstimmt
		for _, kp := range kassenpruefungen_Zeitraum {
			var y int
			for y = 0; y < len(possible_values); y++ {
				if possible_values[y] == kp.Betrag {
					break
				}
			}
			if y == len(possible_values) {
				fehlermeldungen = append(fehlermeldungen, fmt.Sprintf("der Betrag %.2f vom %+v ist keiner der möglichen Ergebnisse. \n\tMögliche Ergebnisse: %+v", kp.Betrag, kp.Datum.Format("02.01.2006"), possible_values))
			}
		}

		kassenstaende = append(kassenstaende, types.Kassenstand{
			Sammeldatum:        ka.Zahlungsdatum,
			Kassenanweisungen:  kass_fuer_datum,
			Kassenstand_beginn: math.Round(kassenstand_tagesbeginn*100) / 100,
			Kassenstand_ende:   math.Round(kassenstand_tagesende*100) / 100,
			Fehlerwert:         fehlerwert,
		})

		// Tagesendsaldo des letzten Datums ist der neue Startsaldo des neuen Datums
		if fehlerwert == types.Fehler {
			kassenstand_tagesbeginn = sum_day_end
		} else {
			kassenstand_tagesbeginn = kassenstand_tagesende
		}

		fehlerwert = types.Korrekt

		if i+n >= len(kassenanweisungen) {
			saldo_end = kassenstand_tagesende
		}
	}

	einnahmen = math.Round(einnahmen*100) / 100
	ausgaben = math.Round(ausgaben*100) / 100

	inhaber := model.GetInhaberByGeldanlageId(anlageId)

	jahresabschluss := types.Jahresabschluss_Geldanlage{
		Fehlermeldungen:  fehlermeldungen,
		Inhaber:          inhaber.Name,
		Kassenpruefungen: kassenpruefungen,
		Geldanlage:       geldanlage,
		Anfangsbetrag:    saldo_start,
		Endbetrag:        saldo_end,
		Ausgaben:         ausgaben,
		Einnahmen:        einnahmen,
		Haushaltsjahr:    haushaltsjahr,
		Kassenstaende:    kassenstaende,
	}

	return jahresabschluss
}

/**
	CalculatePossibleSaldoValues Soll alle möglichen verschiedenen Saldi berechnen, die mit dem Array an Summanden und dem Startwert (dem Startsaldo)
	möglich sind. Das funktioniert, indem die summanden in allen möglichen Kombinationen miteinander addiert werden. Die Ergebnisse dieser Additionen
	werden dann in einen Array eingetragen und zurück gegeben.
	Beispiel: Auf den Startbetrag von 10€ können die Summanden +2€, -5€ und -6€ kommen. Die möglichen Saldi wären dann 12€, 5€, 4€, 7€, -1€, 6€, 1€
	Trick: Der letzte Rechnungsdurchgang kann übersprungen werden, denn das Endergebnis ist bei allen Kombinationen gleich.
	ACHTUNG: Diese Funktion ist exponentiell rechenintensiv! Sie darf nicht mit vielen Summanden verwendet werden.
**/
func CalculatePossibleSaldoValues(beginn float64, summanden []float64, ende float64) []float64 {
	values := []float64{}

	summanden_int := []int64{}
	for _, s := range summanden {
		summanden_int = append(summanden_int, int64(100*s))
	}

	values_int := RecursivelyCalculateIntSums(int64(beginn*100), summanden_int)

	occurred := map[int64]bool{}

	for v := range values_int {
		if !occurred[values_int[v]] {
			occurred[values_int[v]] = true

			values = append(values, float64(values_int[v]))
		}
	}

	values = append(values, beginn)
	values = append(values, ende)
	return values
}

/**
	This function has a fundamental flaw since it is absurdly expensive as soon as the number of possible values get bigger.
	It has to be made moere efficient
**/
func RecursivelyCalculateIntSums(beginn int64, summanden []int64) []int64 {
	values := []int64{}
	for i, s := range summanden {
		values = append(values, s+beginn)
		values = append(values, RecursivelyCalculateIntSums(beginn, removeValue(summanden, i))...)
	}
	return values
}

func removeValue(array []int64, i int) []int64 {
	array[i] = array[len(array)-1] // Kopiere den letzten Wert an die Stelle der Summ
	return array[:len(array)-1]
}

func getGeldSummanden(kas []model.Kassenanweisung_model, anlageId int) []float64 {
	v := []float64{}
	for _, ka := range kas {
		if ka.Geldanlage_Geldempfaenger == anlageId {
			v = append(v, ka.Betrag)
		} else {
			v = append(v, -ka.Betrag)
		}
	}
	return v
}

func GetJahresabschlussHtmlData(jahresabschluss types.Jahresabschluss_Geldanlage) (interface{}, error) {
	lang := language.MustParse(os.Getenv("APP_LANG"))
	p := message.NewPrinter(lang)
	cur, _ := currency.FromTag(lang)
	curSymbol := currency.Symbol(cur)

	// Generate Date for output
	years := strings.Split(jahresabschluss.Haushaltsjahr, "/")
	if len(years) != 2 {
		panic(errors.New("haushaltsjahr is not a valid string: " + jahresabschluss.Haushaltsjahr + " not in the form of 21/22"))
	}
	t1, err := time.Parse("06-01-02", years[0]+"-"+os.Getenv("HAUSHALTSJAHR_BEGIN_MONTH")+"-"+os.Getenv("HAUSHALTSJAHR_BEGIN_DATE"))
	if err != nil {
		return "", err
	}

	t2 := t1.AddDate(1, -1, 0)
	sum := 0
	for i := 0; i < len(jahresabschluss.Kassenstaende); i++ {
		sum += len(jahresabschluss.Kassenstaende[i].Kassenanweisungen)
	}

	data := struct {
		Title            string
		Subtitle         string
		Fehler           []string
		Balance_Start    string
		Earns            string
		Spendings        string
		Balance_End      string
		Balance_End_Red  bool
		Kassenstaende    []types.Kassenstand_jahresabschluss
		Kassenpruefungen []types.Kassenpruefung_jahresabschluss
	}{
		Title:            "Jahresabschluss " + jahresabschluss.Inhaber + " - " + jahresabschluss.Geldanlage.Name,
		Subtitle:         t1.Format("01/2006") + " - " + t2.Format("01/2006"),
		Fehler:           jahresabschluss.Fehlermeldungen,
		Balance_Start:    p.Sprintf("%.2f %v", jahresabschluss.Anfangsbetrag, curSymbol),
		Balance_End_Red:  jahresabschluss.Endbetrag < 0,
		Earns:            p.Sprintf("%.2f %v", jahresabschluss.Einnahmen, curSymbol),
		Spendings:        p.Sprintf("%.2f %v", jahresabschluss.Ausgaben, curSymbol),
		Balance_End:      p.Sprintf("%.2f %v", jahresabschluss.Endbetrag, curSymbol),
		Kassenstaende:    CreateKassenstaendeJahresabschluss(jahresabschluss.Kassenstaende, jahresabschluss.Geldanlage.Id),
		Kassenpruefungen: GetKassenpruefungenForJahresabschluss(jahresabschluss.Kassenpruefungen),
	}

	return data, nil
}
