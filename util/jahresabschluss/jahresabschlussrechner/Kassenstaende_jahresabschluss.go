package jahresabschlussrechner

import (
	"os"

	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/types"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func CreateKassenstaendeJahresabschluss(kass []types.Kassenstand, anlageId int) []types.Kassenstand_jahresabschluss {
	lang := language.MustParse(os.Getenv("APP_LANG"))
	p := message.NewPrinter(lang)
	cur, _ := currency.FromTag(lang)
	kass_jahresabschl := []types.Kassenstand_jahresabschluss{}
	dateFormat := constants.GetDateFormatForLang()

	for _, ks := range kass {
		kass_jahresabschl = append(kass_jahresabschl, types.Kassenstand_jahresabschluss{
			Datum:             ks.Sammeldatum.Format(string(dateFormat)),
			Uebertrag:         p.Sprintf("%.2f %v", ks.Kassenstand_beginn, currency.Symbol(cur)),
			Tagesendbetrag:    p.Sprintf("%v %v", ks.Kassenstand_ende, currency.Symbol(cur)),
			Kassenanweisungen: GetKassenanweisungen_stringFromKassenanweisungen(ks.Kassenanweisungen, anlageId),
			Fehlerwert:        int(ks.Fehlerwert),
		})
	}
	return kass_jahresabschl
}
