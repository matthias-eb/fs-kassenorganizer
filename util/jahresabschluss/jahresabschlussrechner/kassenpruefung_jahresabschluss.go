package jahresabschlussrechner

import (
	"os"

	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/types"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func CreateKassenpruefung_jahresabschluss(kp model.Kassenpruefung) (kpj types.Kassenpruefung_jahresabschluss) {
	lang := language.MustParse(os.Getenv("APP_LANG"))
	p := message.NewPrinter(lang)
	cur, _ := currency.FromTag(lang)
	dateFormat := constants.GetDateFormatForLang()

	geldanlage, err := model.GetGeldanlage(kp.Geldanlage)
	if err != nil {
		panic(err)
	}
	kpj.Geldanlage = geldanlage.Name
	kpj.Datum = kp.Datum.Format(string(dateFormat))
	kpj.Betrag = p.Sprintf("%.2f %v", kp.Betrag, currency.Symbol(cur))

	return
}

func GetKassenpruefungenForJahresabschluss(kpr []model.Kassenpruefung) (kpj []types.Kassenpruefung_jahresabschluss) {
	for _, kp := range kpr {
		kpj = append(kpj, CreateKassenpruefung_jahresabschluss(kp))
	}
	return
}
