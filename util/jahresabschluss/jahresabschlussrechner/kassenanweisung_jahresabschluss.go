package jahresabschlussrechner

import (
	"os"
	"strconv"

	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/types"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func GetKassenanweisungen_stringFromKassenanweisungen(kassenanweisungen []model.Kassenanweisung_model, anlageId int) []types.Kassenanweisung_jahresabschluss {
	output := []types.Kassenanweisung_jahresabschluss{}
	for _, kas := range kassenanweisungen {
		output = append(output, Kassenanweisung_To_Kassenanweisung_jahresabschluss(kas, anlageId))
	}

	return output
}

func Kassenanweisung_To_Kassenanweisung_jahresabschluss(kas model.Kassenanweisung_model, anlageId int) (output types.Kassenanweisung_jahresabschluss) {
	lang := language.MustParse(os.Getenv("APP_LANG"))
	p := message.NewPrinter(lang)
	cur, _ := currency.FromTag(lang)
	dateFormat := constants.GetDateFormatForLang()

	geldgeber := model.GetInhaberByGeldanlageId(kas.Geldanlage_Geldgeber)
	geldempfaenger := model.GetInhaberByGeldanlageId(kas.Geldanlage_Geldempfaenger)
	empfaengeranlage, _ := model.GetGeldanlage(kas.Geldanlage_Geldempfaenger)
	geldgeberanlage, _ := model.GetGeldanlage(kas.Geldanlage_Geldgeber)

	output = types.Kassenanweisung_jahresabschluss{
		Titelnr:        strconv.FormatInt(int64(kas.Titelnr), 10),
		Kategorie:      string(kas.Zahlungsart),
		Geldgeber:      geldgeber.Name + " - " + geldgeberanlage.Name,
		Geldempfaenger: geldempfaenger.Name + " - " + empfaengeranlage.Name,
		Begruendung:    kas.Begruendung,
		Zahlungsdatum:  kas.Zahlungsdatum.Format(string(dateFormat)),
	}

	if kas.IsEinzahlung(anlageId) {
		output.Betrag_einnahme = p.Sprintf("%.2f %v", kas.Betrag, currency.Symbol(cur))
	} else {
		output.Betrag_ausgabe = p.Sprintf("%.2f %v", kas.Betrag, currency.Symbol(cur))
	}

	return
}
