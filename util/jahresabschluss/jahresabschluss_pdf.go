package jahresabschluss

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"text/template"
	"time"

	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/types"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/pdf"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/pdf_filler"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/util_json"

	jr "gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/jahresabschlussrechner"
)

func GetOrCreateJahresabschlussPDF(hhj string, id int64) (string, error) {
	if len(hhj) != 5 {
		return "", errors.New("wrong or unknown formatting for haushaltsjahr. Format like this: 'YY/YY'")
	}
	n1 := hhj[0:2]
	n2 := hhj[3:5]

	t1 := time.Now()
	changes_pdf_1 := true
	fileName, err := GetJahresabschlussPDF(id, hhj)
	if err != nil {
		if err.Error() == constants.ERR_NO_CHANGES {
			changes_pdf_1 = false
		} else {
			panic("Cannot create Jahresabschluss Table PDF. This shouldn't happen unless the program isn't installed properly.")
		}
	}

	kassenanweisungen := model.QueryKassenanweisungenForHaushaltsjahrForGeldanlageOrdered(n1+"/"+n2, int(id), "Titelnr ASC")

	filename_Kassenanweisungen := fmt.Sprintf("%s%d-%s", constants.ID_HHJ_NAME_PREFIX, id, n1+"_"+n2)

	d := time.Since(t1)
	fmt.Println("Time needed for generating a pdf and an html file with the accumulated data: ", d.Milliseconds(), "ms")
	changes_pdf_2 := true
	t2 := time.Now()
	//Create merged PDF and send it back
	mergedKassenanweisung_Path, err := pdf.CreateAndMergePDFs(kassenanweisungen, filename_Kassenanweisungen)
	if err != nil {
		if err.Error() == constants.ERR_NO_CHANGES {
			changes_pdf_2 = false
		} else {
			panic("Cannot create Merged Jahresabschluss PDF. This shouldn't happen unless the program isn't installed properly.")
		}
	}
	d = time.Since(t1)
	d1 := time.Since(t2)
	fmt.Println("Time needed for merging the cass orders: ", d1.Milliseconds(), "ms, elapsed time since t1: ", d.Milliseconds(), "ms")

	filename_All_Merged := fmt.Sprintf("%s_With_KAW", filename_Kassenanweisungen)
	if changes_pdf_1 || changes_pdf_2 {
		t2 = time.Now()
		err = pdf_filler.MergePDFs(constants.PDF_JAHRESABSCHL_FOLDER, filename_All_Merged, []string{constants.PDF_JAHRESABSCHL_FOLDER + fileName, mergedKassenanweisung_Path})
		if err != nil {
			panic("Cannot merge files. This shouldn't happen unless the progrem isn't installed properly.")
		}
		d = time.Since(t1)
		d1 = time.Since(t2)
		fmt.Println("Time needed for merging the overview and cass orders: ", d1.Seconds(), "s, elapsed time since t1: ", d.Seconds(), "s")
	} else {
		fmt.Println("Skipped last merge as it is unnecessary")
	}
	return filename_All_Merged, nil
}

func GetJahresabschlussPDF(id int64, hhj string) (string, error) {
	var filename = "jahresabschluss_" + strconv.FormatInt(id, 10) + "-" + strings.ReplaceAll(hhj, "/", "_")
	var json_path = constants.JSON_JAHRESABSCHL_FOLDER + filename + ".json"
	jahresabschl_struct := jr.GetJahresabschlussForHaushaltsjahrForAnlageId(hhj, int(id))

	json_bytes, err1 := os.ReadFile(json_path)

	marshal_bytes, err2 := json.Marshal(jahresabschl_struct)
	if err2 != nil {
		panic(err2)
	}

	f1, err_not_exists := os.Open(constants.PDF_JAHRESABSCHL_FOLDER + filename + ".pdf")
	if err_not_exists == nil {
		f1.Close()
	}
	f1, err_not_exists2 := os.Open(constants.PDF_JAHRESABSCHL_FOLDER + filename + ".html")
	if err_not_exists2 == nil {
		f1.Close()
	}

	// Only return without changes if the html file, the json file and the pdf file exist and if the json file contains the same bytes as the generated jahresabschluss
	if err1 == nil && string(marshal_bytes) == string(json_bytes) && !os.IsNotExist(err_not_exists) && !os.IsNotExist(err_not_exists2) {
		return filename + ".pdf", errors.New(constants.ERR_NO_CHANGES)
	}

	data, err := jr.GetJahresabschlussHtmlData(jahresabschl_struct)
	if err != nil {
		return "", err
	}
	htmlTemplate := template.Must(template.ParseFiles(constants.PATH_TEMPL_JAHRABSCHL))
	var byteBuffer bytes.Buffer
	err = htmlTemplate.ExecuteTemplate(&byteBuffer, "jahresabschluss.go.tmpl", data)
	if err != nil {
		return "", err
	}

	htmlFilePath := constants.PDF_JAHRESABSCHL_FOLDER + filename + ".html"

	f, err := os.Create(htmlFilePath)
	if err != nil {
		return "", err
	}
	w := bufio.NewWriter(f)
	w.WriteString(byteBuffer.String())
	w.Flush()
	f.Close()

	err = pdf.SaveHtmlAsPDF(htmlFilePath, filename+".pdf")
	if err != nil {
		return "", err
	}
	util_json.CreateJSONFileJahresabschluss(jahresabschl_struct, json_path)
	return filename + ".pdf", nil
}

func RecreatePDFPartially(jahresabschluss_struct types.Jahresabschluss_Geldanlage, outputPath string) {

}
