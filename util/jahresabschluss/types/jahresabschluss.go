package types

import (
	"time"

	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
)

type Fehlerwert int

const (
	Korrekt   Fehlerwert = 0
	Bestätigt Fehlerwert = 1
	Fehler    Fehlerwert = -1
)

type Kassenstand struct {
	Sammeldatum        time.Time
	Kassenanweisungen  []model.Kassenanweisung_model
	Kassenstand_beginn float64
	Kassenstand_ende   float64
	Fehlerwert         Fehlerwert
}

type Jahresabschluss_Geldanlage struct {
	Fehlermeldungen  []string
	Inhaber          string
	Haushaltsjahr    string
	Geldanlage       model.Geldanlage_model
	Anfangsbetrag    float64
	Endbetrag        float64
	Einnahmen        float64
	Ausgaben         float64
	Kassenpruefungen []model.Kassenpruefung
	Kassenstaende    []Kassenstand
}

type Kassenstand_jahresabschluss struct {
	Datum             string
	Uebertrag         string
	Tagesendbetrag    string
	Kassenanweisungen []Kassenanweisung_jahresabschluss
	Fehlerwert        int
}

type Kassenanweisung_jahresabschluss struct {
	Titelnr         string
	Betrag_ausgabe  string
	Betrag_einnahme string
	Kategorie       string
	Geldgeber       string
	Geldempfaenger  string
	Begruendung     string
	Zahlungsdatum   string
}

type Kassenpruefung_jahresabschluss struct {
	Geldanlage string
	Datum      string
	Betrag     string
}
