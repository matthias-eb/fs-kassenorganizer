package pdf_filler

import (
	"strconv"
	"strings"

	pdfinject "bitbucket.org/inceptionlib/pdfinject-go/pdfinject"
	cst "gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	itf "gitlab.com/matthias-eb/fs-kassenorganizer/interfaces"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/util_json"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func FillKassenanweisung_Form(kas itf.Kassenanweisung) map[string]interface{} {
	p := message.NewPrinter(language.German)
	form := map[string]interface{}{
		"Art der *nahme":                 kas.Einnahme_Ausgabe_rb,
		"Haushaltsjahr":                  kas.Haushaltsjahr,
		"Titel-Nr.":                      strconv.FormatInt(int64(kas.Titelnr), 10),
		"Betrag":                         p.Sprintf("%.2f", kas.Betrag),
		"in Worten":                      kas.Betrag_In_Worten,
		"Einzahler Empfänger 1":          kas.Einzahler_Empfänger_1,
		"Einzahler Empfänger 2":          kas.Einzahler_Empfänger_2,
		"Begründung 2":                   kas.Begründung_2,
		"Begründung 1":                   kas.Begründung_1,
		"Rechnung_CB":                    kas.CBValue(kas.Rechnung_cb),
		"Überweisungsbeleg_CB":           kas.CBValue(kas.Überweisungsbeleg_cb),
		"Quittung_CB":                    kas.CBValue(kas.Quittung_cb),
		"Kassenverwaltung_am":            kas.Kassenverwaltung_am,
		"Gebucht_am":                     kas.Gebucht_am,
		"Kassenverwaltung_IBAN_CB":       kas.CBValue(kas.Kassenverwaltung_IBAN_cb),
		"Kassenverwaltung IBAN":          kas.Kassenverwaltung_IBAN,
		"Kreditinstitut":                 kas.Kreditinstitut_Auszahlung,
		"IBAN":                           kas.IBAN_Auszahlung,
		"BIC":                            kas.BIC_Auszahlung,
		"Ort_Datum_AStAMitglied":         kas.Ort_Datum_AStAMitglied,
		"Ort_Datum_Finanzreferentin":     kas.Ort_Datum_Finanzreferentin,
		"Buchführungstitel":              kas.Buchführungstitel,
		"Buchführungsdatum":              kas.Buchführungsdatum,
		"Buchführung Bemerkungen":        kas.Buchführung_Bemerkungen,
		"Buchführung Bemerkungen 2":      kas.Buchführung_Bemerkungen_2,
		"Ort_Datum_Buchhalterin":         kas.Ort_Datum_Buchhalterin,
		"AuszugNr":                       kas.Kassenverwaltung_AuszugNr,
		"Sonstiges_Konto_Nr_CB":          kas.CBValue(kas.Sonstiges_Konto_cb),
		"Sonstiges_Konto_Kontonr":        kas.Sonstiges_Konto_Kontonr,
		"Barkasse_CB":                    kas.CBValue(kas.Kassenverwaltung_Barkasse_cb),
		"Kassenverwaltung Bemerkungen 1": kas.Kassenverwaltung_Bemerkung_1,
		"Kassenverwaltung Bemerkungen 2": kas.Kassenverwaltung_Bemerkung_2,
		"Ort_Datum_Kassenverwaltung":     kas.Ort_Datum_Kassenverwaltung,
	}
	return form
}

func KassenanweisungenToPDF(kassenanweisungen []model.Kassenanweisung_model) (filepaths []string) {
	filepaths = []string{}
	for _, kanw_md := range kassenanweisungen {
		// Compare to JSON File
		pdf_filepath, json_filepath := getKwFilePaths(kanw_md)
		differs := util_json.JsonDiffersKw(json_filepath, kanw_md)

		if differs {
			kanw := kanw_md.GetKassenanweisung()
			form_Data := FillKassenanweisung_Form(kanw)
			util.CreateDirIfNotExists(cst.PDF_FOLDER + strings.ReplaceAll(kanw_md.Haushaltsjahr, "/", "_") + "/")
			pdfInjection := pdfinject.New()
			destFile, err := pdfInjection.FillWithDestFile(form_Data, "Kassenanordnung_2020_2021.pdf", pdf_filepath)
			if err != nil {
				panic(err)
			}
			filepaths = append(filepaths, *destFile)
			util_json.CreateJsonRepresentationKw(kanw_md, json_filepath)
		} else {
			filepaths = append(filepaths, pdf_filepath)
		}
	}
	return
}

// getKwFilePaths returns two string: a pdf and a json path for this kassenanweisung
func getKwFilePaths(kas model.Kassenanweisung_model) (string, string) {
	cst_filepath_part := strings.ReplaceAll(kas.Haushaltsjahr, "/", "_") + "/" + strconv.FormatInt(int64(kas.Titelnr), 10) + "_" + kas.Ausstellungsdatum.Format("2006-01-02")
	return cst.PDF_FOLDER + cst_filepath_part + ".pdf", cst.JSON_FOLDER + cst_filepath_part + ".json"
}

func KassenanweisungToFilepath(kas model.Kassenanweisung_model) string {
	return cst.PDF_FOLDER + strings.ReplaceAll(kas.Haushaltsjahr, "/", "_") + "/" + strconv.FormatInt(int64(kas.Titelnr), 10) + "_" + kas.Ausstellungsdatum.Format("2006-01-02") + ".pdf"
}

func MergePDFs(dir string, outputName string, filepaths []string) error {
	return pdfinject.MergePDF(dir, outputName, filepaths...)
}
