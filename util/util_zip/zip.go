package util_zip

import (
	"archive/zip"
	"encoding/json"
	"io"
	"os"
	"path/filepath"

	cst "gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	mdl "gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/pdf_filler"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/util_json"
)

// addMissingKassenanweisungenToZip checks for differences in the given json file and the Kassenanweisungen. If there are some,
// the Zip file will be recreated with the correct Kassenanweisungen together with the json file.
func AddMissingKassenanweisungenToZip(kassenanweisungen []mdl.Kassenanweisung_model, zip_name string, json_name string) (err error) {
	json_bytes, err := os.ReadFile(cst.JSON_FOLDER + json_name)
	if err != nil {
		return
	}

	marshal_bytes, err := json.Marshal(kassenanweisungen)
	if err != nil {
		return
	}

	// Compare the kassenanweisungen Json with the one from the Json File.
	// If the content is equal, serve the Zip file immediately, otherwise, add only the missing content
	if string(marshal_bytes) == string(json_bytes) {
		return
	}

	// Content is not equal, fill in kassenanweisung array to compare each one
	kassenanweisungen_json := []mdl.Kassenanweisung_model{}
	err = json.Unmarshal(json_bytes, &kassenanweisungen_json)
	if err != nil {
		return
	}

	paths := []string{}

	for i, ka := range kassenanweisungen {
		if !ka.Equals(kassenanweisungen_json[i]) {
			pdf_filler.KassenanweisungenToPDF(append([]mdl.Kassenanweisung_model{}, ka))
		}
		paths = append(paths, pdf_filler.KassenanweisungToFilepath(ka))
	}

	//Create new Zip file with Json
	CreateZipAndJsonFile(paths, kassenanweisungen, zip_name, json_name)

	return
}

func CreateZipAndJsonFile(filepaths []string, kassenanweisungen []mdl.Kassenanweisung_model, zip_name string, json_name string) {

	//CompressToZip call
	CompressToZip(cst.ZIP_FOLDER+zip_name, filepaths, cst.REL_ROOT)

	//Create JSON file
	util_json.CreateJsonRepresentation(kassenanweisungen, cst.JSON_FOLDER+json_name)
}

// CompressToZip creates a file at the path of `outputFile` and inserts all filepaths in `files` into the
// Zip with the path relative to the `relRoot` Directory path.
//
// For example, if the relRoot is '/home/xy' and the outputFile is '/root/xyz.zip' and one file with the path
// '/home/xy/z/asd.txt' should be added, '/home/xy' is omitted in the filepath and the new relative Path inside
// of the zip file is 'z/asd.txt'. The zip file is then saved to '/root/xyz.zip'
func CompressToZip(outputFile string, files []string, relRoot string) (err error) {
	newZipFile, err := os.Create(outputFile)
	if err != nil {
		return
	}
	defer newZipFile.Close()

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

	for _, file := range files {
		if err = AddFileToZip(zipWriter, file, filepath.Dir(relRoot)); err != nil {
			return
		}
	}

	zipWriter.Flush()
	newZipFile.Sync()

	return
}

func AddFileToZip(zipWriter *zip.Writer, filename string, relRootDir string) (err error) {
	fileToZip, err := os.Open(filename)

	if err != nil {
		return
	}
	defer fileToZip.Close()

	relPath, err := filepath.Rel(relRootDir, filename)
	if err != nil {
		return err
	}

	writer, err := zipWriter.Create(relPath)
	if err != nil {
		return
	}

	_, err = io.Copy(writer, fileToZip)
	zipWriter.Flush()

	return
}
