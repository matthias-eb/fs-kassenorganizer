package pdf_editor

import (
	"fmt"
	"os"

	pdfcpu "github.com/pdfcpu/pdfcpu/pkg/api"
)

func PDF_Remove_Pages(pages_to_remove []string, filepath string) error {
	err := pdfcpu.RemovePagesFile(filepath, filepath, pages_to_remove, nil)
	if err != nil {
		return err
	}
	return nil
}

func Info(filepath string, pages []string) {
	strs, err := pdfcpu.InfoFile(filepath, pages, nil)
	if err != nil {
		fmt.Println("Error: " + err.Error())
	} else {
		fmt.Println(strs)
	}
}

func PDF_Add_Pages(pages_to_add []string, outputPath string, append_pages bool) error {
	if !append_pages {
		err := os.Remove(outputPath)
		if err != nil {
			return err
		}
	}
	err := pdfcpu.MergeAppendFile(pages_to_add, outputPath, nil)
	if err != nil {
		return err
	}
	return nil
}
