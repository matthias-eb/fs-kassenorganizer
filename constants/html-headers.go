package constants

const HEADER_ACCEPT = "Accept"

const HEADER_APPL_JSON = "application/json"
const HEADER_APPL_ZIP = "application/zip"
const HEADER_ALL = "*/*"
const HEADER_PDF = "application/pdf"
