package constants

import (
	"os"

	language "golang.org/x/text/language"
)

type ENV_VARS string

const (
	InhaberName   ENV_VARS = "INHABER_NAME"
	DBName        ENV_VARS = "DB_NAME"
	DBTableName   ENV_VARS = "DB_TABLE_NAME"
	DBUser        ENV_VARS = "DB_USER"
	DBPw          ENV_VARS = "DB_PW"
	DBIp          ENV_VARS = "DB_IP"
	DBPort        ENV_VARS = "DB_PORT"
	HhjStartTag   ENV_VARS = "HAUSHALTSJAHR_BEGIN_DATE"
	HhjStartMonat ENV_VARS = "HAUSHALTSJAHR_BEGIN_MONTH"
	AppSprache    ENV_VARS = "APP_LANG"
	Ort           ENV_VARS = "ORT"
)

type DateFormat string

const (
	German          DateFormat = "02.01.2006"
	AmericanEnglish DateFormat = "01/02/2006"
	English         DateFormat = "2006-01-02"
)

func GetDateFormatForLang() DateFormat {
	lang := language.MustParse(os.Getenv("APP_LANG"))

	switch lang {
	case language.German:
		return German
	case language.English:
		return English
	case language.AmericanEnglish:
		return AmericanEnglish
	}
	return English
}

const ERR_NO_CHANGES = "no changes"
const ID_HHJ_NAME_PREFIX = "id_hhj_"
