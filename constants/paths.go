package constants

import (
	"net/http"
	"os"
)

var REL_ROOT = os.Getenv("HOME") + "/fs-kassenorganizer/"
var ZIP_FOLDER = REL_ROOT + "zip_files/"
var PDF_FOLDER = REL_ROOT + "pdf_files/"
var PDF_MERGE_FOLDER = PDF_FOLDER + "merged/"
var PDF_JAHRESABSCHL_FOLDER = PDF_FOLDER + "jahresabschluss/"

var ZIP_FS http.FileSystem = http.Dir(ZIP_FOLDER)
var PDF_JABSCHL_FS = http.Dir(PDF_JAHRESABSCHL_FOLDER)
var PDF_MERGE_FS http.FileSystem = http.Dir(PDF_MERGE_FOLDER)

var PATH_TEMPL_JAHRABSCHL = "templates/jahresabschluss.go.tmpl"

var JSON_FOLDER = REL_ROOT + "json_files/"
var JSON_MERGE_FOLDER = JSON_FOLDER + "merged/"
var JSON_JAHRESABSCHL_FOLDER = JSON_FOLDER + "jahresabschluss/"
