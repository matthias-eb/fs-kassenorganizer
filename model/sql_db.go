package model

import (
	"fmt"
	"os"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Tabler interface {
	TableName() string
}

var DB *gorm.DB
var DB_USER string
var DB_PW string
var DB_IP string
var DB_PORT string
var DB_NAME string

func init() {
	DB_USER = os.Getenv("DB_USER")
	DB_PW = os.Getenv("DB_PW")
	DB_IP = os.Getenv("DB_IP")
	DB_PORT = os.Getenv("DB_PORT")
	DB_NAME = os.Getenv("DB_NAME")

	var err error
	dsn := fmt.Sprintf("%+v:%+v@tcp(%+v:%+v)/%+v?charset=utf8mb4&parseTime=True&loc=Local", DB_USER, DB_PW, DB_IP, DB_PORT, DB_NAME)
	fmt.Println("The following sql path is opened: " + dsn)
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("Init done")
}
