package model

import (
	"errors"
	"fmt"
)

type Geldanlage_model struct {
	Id        int `gorm:"primaryKey"`
	Name      string
	Konto     bool
	IBan      string
	InhaberId int64
	Bank      string
	BIC       string
}

func (Geldanlage_model) TableName() string {
	return "Geldanlagen"
}

func GetGeldanlagen() []Geldanlage_model {
	geldanlagen := []Geldanlage_model{}
	result := DB.Find(&geldanlagen)
	if result.Error != nil {
		fmt.Println(result.Error.Error())
		return []Geldanlage_model{}
	}
	return geldanlagen
}

func GetGeldanlagenByInhaber(inhaberId int) []Geldanlage_model {
	geldanlagen := []Geldanlage_model{}
	result := DB.Find(&geldanlagen, "inhaberId = ?", inhaberId)
	if result.Error != nil {
		fmt.Println(result.Error.Error())
		return []Geldanlage_model{}
	}
	return geldanlagen
}

func GetGeldanlage(anlageId int) (geldanlage Geldanlage_model, err error) {
	result := DB.Find(&geldanlage, anlageId)
	err = result.Error
	if result.RowsAffected < 1 {
		err = errors.New("no rows found")
	}
	return
}
