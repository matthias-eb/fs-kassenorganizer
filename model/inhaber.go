package model

type Inhaber_Model struct {
	Id   int64 `gorm:"primaryKey"`
	Name string
}

func (Inhaber_Model) TableName() string {
	return "Inhaber"
}

func GetInhaber(id int64) (inh Inhaber_Model) {
	result := DB.First(&inh, "Id = ?", id)
	if result.Error != nil {
		panic(result.Error)
	}
	return
}

func GetInhaberByName(name string) (inh Inhaber_Model) {
	result := DB.First(&inh, "Name = ?", name)
	if result.Error != nil {
		panic(result.Error)
	}
	return
}

func GetInhaberByGeldanlageId(anlageId int) (inh Inhaber_Model) {
	geldanlage := Geldanlage_model{}
	result := DB.Table("Geldanlagen").First(&geldanlage, "Id = ?", anlageId)
	if result.Error != nil {
		panic(result.Error)
	}
	result = DB.Where("Inhaber.Id = ?", geldanlage.InhaberId).First(&inh)
	if result.Error != nil {
		panic(result.Error)
	}
	return
}
