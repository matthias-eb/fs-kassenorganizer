package model

import (
	"fmt"
	"os"
	"strings"
	"time"
)

type Kassenpruefung struct {
	Id         int `gorm:"primaryKey"`
	Datum      time.Time
	Betrag     float64
	Geldanlage int
}

func (Kassenpruefung) TableName() string {
	return "Kassenpruefungen"
}

func GetAllKassenpruefungen(direction string) (kps []Kassenpruefung, err error) {
	if strings.ToLower(direction) == "desc" {
		direction = "DESC"
	} else {
		direction = "ASC"
	}
	result := DB.Order("Datum " + direction).Find(&kps)
	if result.Error != nil {
		err = result.Error
	}
	return
}

func GetKassenpruefungById(kpId int) (kp Kassenpruefung, err error) {
	result := DB.Find(&kp, kpId)
	if result.Error != nil {
		err = result.Error
	}
	return
}

func GetKassenpruefungenForAnlageort(anlageId int) ([]Kassenpruefung, error) {
	kassenpruefungen := []Kassenpruefung{}
	result := DB.Find(&kassenpruefungen, "Geldanlage = ?", anlageId)
	if result.Error != nil {
		fmt.Printf("Error found: %+v\n", result.Error)
		return nil, result.Error
	}
	return kassenpruefungen, nil
}

func GetLatestKassenpruefung() (kpr Kassenpruefung, e error) {
	result := DB.Order("Datum DESC").First(&kpr)
	if result.Error != nil {
		fmt.Printf("Error found: %+v\n", result.Error)
		e = result.Error
	}
	return
}

func GetKassenpruefungenForAnlageortInHaushaltsjahr(anlageId int, haushaltsjahr string) ([]Kassenpruefung, error) {
	HJ_BEGIN_MONTH := os.Getenv("HAUSHALTSJAHR_BEGIN_MONTH")
	HJ_BEGIN_DATE := os.Getenv("HAUSHALTSJAHR_BEGIN_DATE")
	kassenpruefungen := []Kassenpruefung{}
	jahr_split := strings.Split(haushaltsjahr, "/")
	date_begin, err := time.Parse("02.01.06", HJ_BEGIN_DATE+"."+HJ_BEGIN_MONTH+"."+jahr_split[0])
	if err != nil {
		panic(err)
	}
	date_begin = date_begin.AddDate(0, 0, -1)
	date_end, err := time.Parse("02.01.06", HJ_BEGIN_DATE+"."+HJ_BEGIN_MONTH+"."+jahr_split[1])
	if err != nil {
		panic(err)
	}
	date_end = date_end.AddDate(0, 0, -1)
	result := DB.Where("Geldanlage = ? AND Datum BETWEEN ? AND ?", anlageId, date_begin, date_end).Order("Datum ASC").Find(&kassenpruefungen)
	if result.Error != nil {
		fmt.Printf("Error found:%+v\n", result.Error)
		return nil, result.Error
	}
	return kassenpruefungen, nil
}

func CreateKassenpruefung(kp_model *Kassenpruefung) error {
	res := DB.Create(kp_model)
	if res.Error != nil {
		return res.Error
	}
	return nil
}

func UpdateKassenpruefung(kp_model *Kassenpruefung) error {
	currentKP := Kassenpruefung{}
	res := DB.First(&currentKP, kp_model.Id)
	if res.Error != nil {
		return res.Error
	}
	res = DB.Model(&currentKP).Updates(kp_model)
	if res.Error != nil {
		return res.Error
	}
	return nil
}

func DeleteKassenpruefung(kp_model Kassenpruefung) error {
	res := DB.Delete(&kp_model)
	if res.Error != nil {
		return res.Error
	}
	return nil
}
