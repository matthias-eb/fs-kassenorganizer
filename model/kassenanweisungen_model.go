package model

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	itf "gitlab.com/matthias-eb/fs-kassenorganizer/interfaces"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util"
	"gorm.io/gorm"
)

const ENV_ORT string = "ORT"

type Zahlungsart string

const (
	Überweisung        Zahlungsart = "Überweisung"
	Bargeldeinzahlung  Zahlungsart = "Bargeldeinzahlung"
	Münzgeldeinzahlung Zahlungsart = "Münzgeldeinzahlung"
	Bargeldauszahlung  Zahlungsart = "Bargeldauszahlung"
	Scheckeinreichung  Zahlungsart = "Scheckeinreichung"
	Tresoreinzahlung   Zahlungsart = "Tresoreinzahlung"
)

type Kassenanweisung_model struct {
	Id                        int    `gorm:"primaryKey"`
	Haushaltsjahr             string `gorm:"uniqueIndex:hhj_tnr,"`
	Titelnr                   int    `gorm:"hhj_tnr,autoIncrement"`
	Begruendung               string
	Betrag                    float64
	Zahlungsart               Zahlungsart
	Beleg                     string
	Ausstellungsdatum         time.Time
	Zahlungsdatum             time.Time
	Geldanlage_Geldempfaenger int
	Geldanlage_Geldgeber      int
}

func (Kassenanweisung_model) TableName() string {
	return "Kassenanweisungen"
}

func (ka1 Kassenanweisung_model) Equals(ka2 Kassenanweisung_model) bool {
	return ka1.Id == ka2.Id &&
		ka1.Haushaltsjahr == ka2.Haushaltsjahr &&
		ka1.Titelnr == ka2.Titelnr &&
		ka1.Begruendung == ka2.Begruendung &&
		ka1.Betrag == ka2.Betrag &&
		ka1.Zahlungsart == ka2.Zahlungsart &&
		ka1.Beleg == ka2.Beleg &&
		ka1.Ausstellungsdatum == ka2.Ausstellungsdatum &&
		ka1.Zahlungsdatum == ka2.Zahlungsdatum &&
		ka1.Geldanlage_Geldempfaenger == ka2.Geldanlage_Geldempfaenger &&
		ka1.Geldanlage_Geldgeber == ka2.Geldanlage_Geldgeber
}

func (kam Kassenanweisung_model) Stringify() string {
	bts, err := json.MarshalIndent(kam, "", "\t")
	if err != nil {
		panic(err)
	}
	return string(bts)
}

func (kam *Kassenanweisung_model) GetKassenanweisung() itf.Kassenanweisung {
	kas := itf.Kassenanweisung{
		Haushaltsjahr: kam.Haushaltsjahr,
		Titelnr:       kam.Titelnr,
		Betrag:        kam.Betrag,
	}
	kas.FillBetragInWorten()
	kas.Begründung_1, kas.Begründung_2 = util.SplitAtMaxBreakpointWord(kam.Begruendung, 95)
	kas.Kassenverwaltung_am = kam.Zahlungsdatum.Format("02.01.2006")
	kas.Ort_Datum_Finanzreferentin = os.Getenv(ENV_ORT) + ", " + kam.Ausstellungsdatum.Format("02.01.2006")
	zahlungsarten := strings.Split(string(kam.Zahlungsart), ", ")
	for _, zahlungsart := range zahlungsarten {
		switch Zahlungsart(zahlungsart) {
		case Überweisung:
			kas.Überweisungsbeleg_cb = true
			kas.Rechnung_cb = true
		case Bargeldauszahlung:
			kas.Quittung_cb = true
			kas.Rechnung_cb = true
		case Bargeldeinzahlung, Münzgeldeinzahlung:
			// Möglicherweise Einzahlung aufs Konto, vielleicht auch Einzahlung in Tresor
			kas.Überweisungsbeleg_cb = true
			kas.Quittung_cb = true
		case Tresoreinzahlung:
			kas.Kassenverwaltung_Barkasse_cb = true
		}
	}

	// Get Inhaber for Geldgeber and Geldempfänger
	empfaenger := GetInhaberByGeldanlageId(kam.Geldanlage_Geldempfaenger)
	geldgeber := GetInhaberByGeldanlageId(kam.Geldanlage_Geldgeber)

	if empfaenger.Name == os.Getenv("INHABER_NAME") && geldgeber.Name != os.Getenv("INHABER_NAME") {
		kas.Einzahler_Empfänger_1, kas.Einzahler_Empfänger_2 = util.SplitAtMaxBreakpointWord(empfaenger.Name, 100)
		kas.Einnahme_Ausgabe_rb = itf.Einnahme
	} else if geldgeber.Name == os.Getenv("INHABER_NAME") && empfaenger.Name != os.Getenv("INHABER_NAME") {
		kas.Einnahme_Ausgabe_rb = itf.Ausgabe
		kas.Einzahler_Empfänger_1, kas.Einzahler_Empfänger_2 = util.SplitAtMaxBreakpointWord(geldgeber.Name, 100)
	} else {
		//Transfer von einem Konto zum nächsten oder Inhaber ist nicht beteiligt, Angabe von Einnahme oder Ausgabe ist egal und auf Einnahme festgesetzt
		kas.Einnahme_Ausgabe_rb = itf.Einnahme
		kas.Einzahler_Empfänger_1, kas.Einzahler_Empfänger_2 = util.SplitAtMaxBreakpointWord(empfaenger.Name, 100)
	}

	if (kas.Überweisungsbeleg_cb) || (kas.Einnahme_Ausgabe_rb == itf.Einnahme && kas.Überweisungsbeleg_cb && (contains(zahlungsarten, string(Münzgeldeinzahlung)) || contains(zahlungsarten, string(Bargeldeinzahlung)))) {
		var geldanlage Geldanlage_model
		var err error
		if kas.Einnahme_Ausgabe_rb == itf.Einnahme {
			geldanlage, err = GetGeldanlage(kam.Geldanlage_Geldempfaenger)
			if err != nil {
				panic(err)
			}
		} else {
			geldanlage, err = GetGeldanlage(kam.Geldanlage_Geldgeber)
			if err != nil {
				panic(err)
			}
		}
		if geldanlage.Konto {
			kas.Kassenverwaltung_IBAN = geldanlage.IBan
			kas.Kassenverwaltung_IBAN_cb = true
			if kas.Einnahme_Ausgabe_rb == itf.Ausgabe {
				kas.IBAN_Auszahlung = kas.Kassenverwaltung_IBAN
				kas.Kreditinstitut_Auszahlung = geldanlage.Bank
				kas.BIC_Auszahlung = geldanlage.BIC
			}
		} else {
			kas.Überweisungsbeleg_cb = false
		}
	} else if kas.Einnahme_Ausgabe_rb == itf.Ausgabe && contains(zahlungsarten, string(Bargeldauszahlung)) {
		kas.Kassenverwaltung_Barkasse_cb = true
	}
	return kas
}

func (kam *Kassenanweisung_model) IsEinzahlung(anlageId_Geldempfaenger int) bool {
	if kam.Geldanlage_Geldempfaenger != anlageId_Geldempfaenger && kam.Geldanlage_Geldgeber != anlageId_Geldempfaenger {
		panic(errors.New("geldanlage is not part of this transaction"))
	}

	return kam.Geldanlage_Geldempfaenger == anlageId_Geldempfaenger
}

func GetKassenanweisung(id int) (Kassenanweisung_model, error) {
	ka := Kassenanweisung_model{}
	result := DB.Find(&ka, id)
	if ka == (Kassenanweisung_model{}) {
		return ka, gorm.ErrRecordNotFound
	}
	if result.Error != nil {
		panic(result.Error)
	}
	return ka, nil
}

func GetMultipleKassenanweisung(ids []int) ([]Kassenanweisung_model, error) {
	kass_mdls := []Kassenanweisung_model{}
	if len(ids) == 0 {
		return kass_mdls, nil
	}
	result := DB.Find(&kass_mdls, ids)

	if result.RowsAffected == 0 {
		return kass_mdls, gorm.ErrRecordNotFound
	}
	if result.Error != nil {
		panic(result.Error)
	}
	return kass_mdls, nil
}

func GetHaushaltsjahre() []string {
	var haushaltsjahre []string = []string{}
	result := DB.Table(Kassenanweisung_model{}.TableName()).Select("Haushaltsjahr").Group("Haushaltsjahr").Scan(&haushaltsjahre)
	if result.Error != nil {
		fmt.Println(result.Error.Error())
		return []string{}
	}
	return haushaltsjahre
}

func HasHaushaltsjahr(hhj string) bool {
	haushaltsjahr := ""
	result := DB.Table(Kassenanweisung_model{}.TableName()).Select("Haushaltsjahr").Group("Haushaltsjahr").Where("Haushaltsjahr = ?", hhj).Scan(&haushaltsjahr)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return false
		} else {
			panic(result.Error)
		}
	} else if result.RowsAffected == 0 {
		return false
	}
	return true
}

func GetTitelnummernForHaushaltsjahr(haushaltsjahr string) []int {
	var titelnummern []int = []int{}
	result := DB.Raw("SELECT Titelnr FROM Kassenanweisungen WHERE Haushaltsjahr = ?", haushaltsjahr).Scan(&titelnummern)
	if result.Error != nil {
		fmt.Println(result.Error.Error())
		return []int{}
	}
	return titelnummern
}

func GetAllKassenanweisungen() []Kassenanweisung_model {
	return queryKassenanweisungWhere("")
}

func GetKassenanweisungenBetweenDates(after *time.Time, before *time.Time) (kas []Kassenanweisung_model) {
	result := DB.Order("Zahlungsdatum").Find(&kas, "Zahlungsdatum > ? AND Zahlungsdatum < ?", after, before)
	if result.Error != nil {
		panic(result.Error)
	}
	return
}

func GetKassenanweisungenAfterDate(after *time.Time) (kas []Kassenanweisung_model) {
	result := DB.Order("Zahlungsdatum").Find(&kas, "Zahlungsdatum > ?", after)
	if result.Error != nil {
		panic(result.Error)
	}
	return
}

func GetKassenanweisungenBeforeDate(before *time.Time) (kas []Kassenanweisung_model) {
	result := DB.Order("Zahlungsdatum").Find(&kas, "Zahlungsdatum < ?", before)
	if result.Error != nil {
		panic(result.Error)
	}
	return
}

func QueryKassenanweisungenForHaushaltsjahrForGeldanlageOrdered(haushaltsjahr string, anlageId int, order string) []Kassenanweisung_model {
	kas := []Kassenanweisung_model{}
	result := DB.Where("Haushaltsjahr = ? AND (Geldanlage_Geldempfaenger = ? OR Geldanlage_Geldgeber = ?)", haushaltsjahr, anlageId, anlageId).Order(order).Find(&kas)
	if result.Error != nil {
		panic(result.Error)
	}
	return kas
}

func QueryKassenanweisungForHaushaltsjahr(haushaltsjahr string) []Kassenanweisung_model {
	return queryKassenanweisungWhere(fmt.Sprintf("Haushaltsjahr='%+v'", haushaltsjahr))
}

func QueryKassenanweisungAfterTitleNrForHaushaltsjahr(haushaltsjahr string, titlenr int) []Kassenanweisung_model {
	return queryKassenanweisungWhere(fmt.Sprintf("Haushaltsjahr='%+v' AND Titelnr > %+v", haushaltsjahr, titlenr))
}

func QueryKassenanweisungTitleNrForHaushaltsjahr(haushaltsjahr string, titelnummern []string) []Kassenanweisung_model {
	queryString := "Haushaltsjahr=" + haushaltsjahr + " AND ("
	for i, tnr := range titelnummern {
		queryString += "Titelnr=" + tnr
		if i < len(titelnummern)-1 {
			queryString += " OR "
		}
	}
	queryString += ")"
	return queryKassenanweisungWhere(queryString)
}

func queryKassenanweisungWhere(where_statement string) []Kassenanweisung_model {
	kassenanweisungen := []Kassenanweisung_model{}
	result := DB.Table(Kassenanweisung_model{}.TableName()).Where(where_statement).Scan(&kassenanweisungen)
	if result.Error != nil {
		fmt.Println(result.Error.Error())
		return []Kassenanweisung_model{}
	}
	return kassenanweisungen
}

func contains(strs []string, searchTerm string) bool {
	for _, s := range strs {
		if s == searchTerm {
			return true
		}
	}
	return false
}

func Find(needle Kassenanweisung_model, heystack []Kassenanweisung_model) int {
	for i, ka := range heystack {
		if ka.Equals(needle) {
			return i
		}
	}
	return -1
}
