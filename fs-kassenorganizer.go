package main

import (
	"os"

	"gitlab.com/matthias-eb/fs-kassenorganizer/initialize"
	"gitlab.com/matthias-eb/fs-kassenorganizer/runners"
)

var isInitialized = initialize.Initialize() // Stellt die Reihenfolge sicher, in der init() aufgerufen wird

func main() {
	started := false
	for _, v := range os.Args {
		switch v {
		case "--Console", "-C":
			started = true
			runners.ConsoleMenu()
		}
	}
	if !started {
		started = true
		runners.StartRestAPI()
	}
}
