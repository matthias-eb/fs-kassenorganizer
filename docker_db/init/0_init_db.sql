use Fachschaft_ET;

-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE TABLE `Geldanlagen` (
  `Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Konto` tinyint(1) NOT NULL COMMENT 'Boolean für Identifikation von Konten',
  `IBan` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `InhaberId` bigint(20) NOT NULL COMMENT 'Zugehöriger Inhaber',
  `Bank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BIC` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `InhaberId` (`InhaberId`),
  CONSTRAINT `Geldanlagen_ibfk_1` FOREIGN KEY (`InhaberId`) REFERENCES `Inhaber` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `Inhaber` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Geschäftspartner in Kassenanweisungen';


CREATE TABLE `Kassenanweisungen` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `Haushaltsjahr` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Titelnr` int(11) NOT NULL,
  `Geldanlage_Geldgeber` bigint(20) NOT NULL,
  `Geldanlage_Geldempfaenger` bigint(20) NOT NULL,
  `Betrag` decimal(10,2) NOT NULL,
  `Begruendung` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Zahlungsdatum` date NOT NULL,
  `Ausstellungsdatum` date NOT NULL,
  `Zahlungsart` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Beleg` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Haushaltsjahr_Titelnr` (`Haushaltsjahr`,`Titelnr`),
  KEY `Geldanlage_Geldgeber` (`Geldanlage_Geldgeber`),
  KEY `Geldanlage_Geldempfaenger` (`Geldanlage_Geldempfaenger`),
  CONSTRAINT `Kassenanweisungen_ibfk_1` FOREIGN KEY (`Geldanlage_Geldgeber`) REFERENCES `Geldanlagen` (`Id`),
  CONSTRAINT `Kassenanweisungen_ibfk_2` FOREIGN KEY (`Geldanlage_Geldempfaenger`) REFERENCES `Geldanlagen` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `Kassenpruefungen` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Datum` datetime NOT NULL,
  `Betrag` decimal(10,2) NOT NULL,
  `Geldanlage` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Geldanlage` (`Geldanlage`),
  CONSTRAINT `Kassenpruefungen_ibfk_1` FOREIGN KEY (`Geldanlage`) REFERENCES `Geldanlagen` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Die Prüfungen der Fachschaftskassen werden hier gespeichert. Jedes Haushaltsjahr braucht pro Geldanlage mindestens eine Kassenprüfung, um den Jahresabschluss zu errechnen';


-- 2022-02-22 22:05:47
