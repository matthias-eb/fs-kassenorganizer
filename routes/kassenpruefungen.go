package routes

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/routes/structs"
)

func RegisterKassenpruefungenHandlers(group *gin.RouterGroup) {
	group.GET("/:id", getKassenpruefung)
	group.POST("/:id", updateKassenpruefung)
	group.GET("/latest", getLatestKassenpruefung)
	group.POST("/", postKassenpruefung)
	group.GET("/", getAllKassenpruefungen)
	group.DELETE("/:id", deleteKassenpruefung)
}

func getLatestKassenpruefung(c *gin.Context) {
	kpr, err := model.GetLatestKassenpruefung()
	if err != nil {
		c.AbortWithError(http.StatusNotFound, err)
		return
	}
	c.JSON(http.StatusOK, kpr)
}

func getKassenpruefung(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	kp, err := model.GetKassenpruefungById(int(id))
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	kpp := structs.CreateFromKassenpruefung(kp)
	c.JSON(http.StatusOK, kpp)
}

func getAllKassenpruefungen(c *gin.Context) {
	direction, _ := c.GetQuery("direction")
	kps, err := model.GetAllKassenpruefungen(direction)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	kpps := []structs.Kassenpruefung_JSON{}
	for _, kp := range kps {
		kpps = append(kpps, structs.CreateFromKassenpruefung(kp))
	}
	c.JSON(http.StatusOK, kpps)
}

func postKassenpruefung(c *gin.Context) {
	kpp := structs.Kassenpruefung_JSON{}
	err := c.BindJSON(&kpp)
	if err != nil {
		fmt.Println("Cannot bind to struct")
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	kp_model, err := kpp.CreateModel()
	if err != nil {
		fmt.Println("Errors while creating model")
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	err = model.CreateKassenpruefung(&kp_model)
	if err != nil {
		fmt.Println("Errors while creating Kassenpruefung")
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.Status(http.StatusOK)
}

func updateKassenpruefung(c *gin.Context) {
	id := c.Param("id")
	kpp := structs.Kassenpruefung_JSON{}
	err := c.BindJSON(&kpp)
	if err != nil {
		fmt.Println("Unable to bind body to struct. Received ", c.Request.Body)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	id_int64, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		fmt.Println("Id was no integer value. \n", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	kp_model, err := kpp.CreateModel()
	if err != nil {
		fmt.Println("Errors while creating model")
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	kp_model.Id = int(id_int64)
	err = model.UpdateKassenpruefung(&kp_model)
	if err != nil {
		fmt.Println("Unable to update Kassenpruefung:")
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	c.Status(http.StatusOK)
}

func deleteKassenpruefung(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	kp, err := model.GetKassenpruefungById(int(id))
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	err = model.DeleteKassenpruefung(kp)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.Status(http.StatusOK)
}
