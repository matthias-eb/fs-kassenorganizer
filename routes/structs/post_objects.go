package structs

import (
	"errors"
	"fmt"
	"os"
	"time"

	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
)

type Kassenpruefung_JSON struct {
	Id             int     `json:"Id,omitempty"`
	Geldanlagename string  `json:"Geldanlagename,omitempty"`
	GeldanlageId   int     `json:"GeldanlageId,omitempty"`
	Betrag         float64 `json:"Betrag"`
	Datum          string  `json:"Datum"`
}

func CreateFromKassenpruefung(kp model.Kassenpruefung) Kassenpruefung_JSON {
	ga, _ := model.GetGeldanlage(kp.Geldanlage)
	return Kassenpruefung_JSON{
		Id:             kp.Id,
		Geldanlagename: ga.Name,
		GeldanlageId:   kp.Geldanlage,
		Betrag:         kp.Betrag,
		Datum:          kp.Datum.Format("02.01.2006"),
	}
}

func (kpp *Kassenpruefung_JSON) CreateModel() (model.Kassenpruefung, error) {
	kp_model := model.Kassenpruefung{}
	err := kpp.CheckForErrors()
	if err != nil {
		return kp_model, err
	}
	kp_model.Betrag = kpp.Betrag
	kp_model.Datum, _ = time.Parse("02.01.2006", kpp.Datum)
	kp_model.Geldanlage = kpp.GeldanlageId

	return kp_model, nil
}

func (kpp *Kassenpruefung_JSON) CheckForErrors() error {
	if kpp.GeldanlageId == 0 {
		inhaber := model.GetInhaberByName(os.Getenv(string(constants.InhaberName)))
		geldanlagen := model.GetGeldanlagenByInhaber(int(inhaber.Id))
		found := false
		for _, ga := range geldanlagen {
			if ga.Name == kpp.Geldanlagename {
				found = true
			}
		}
		if !found {
			return errors.New(fmt.Sprintln("Cannot find Geldanlage with the name \"", kpp.Geldanlagename, "\" for the owner \"", inhaber.Name, "\""))
		}
	} else {
		ga, err := model.GetGeldanlage(kpp.GeldanlageId)
		if err != nil {
			return errors.New(fmt.Sprintln("could not find geldanlage with id ", kpp.GeldanlageId))
		}
		if ga.Name != kpp.Geldanlagename && kpp.Geldanlagename != "" {
			return errors.New(fmt.Sprintln("found geldanlage with id ", kpp.GeldanlageId, " but the Name ", kpp.Geldanlagename, " does not match the found name ", ga.Name))
		} else if kpp.Geldanlagename == "" {
			kpp.Geldanlagename = ga.Name
		}
	}

	_, err := time.Parse("02.01.2006", kpp.Datum)
	if err != nil {
		return err
	}

	return nil
}
