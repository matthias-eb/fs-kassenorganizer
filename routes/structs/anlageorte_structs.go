package structs

import "gitlab.com/matthias-eb/fs-kassenorganizer/model"

type Anlageort_Kassenpruefungen struct {
	AnlageId         int
	Name             string
	Ort              string
	Konto            bool
	IBan             string
	Kassenpruefungen []model.Kassenpruefung
}
