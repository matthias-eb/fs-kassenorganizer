package routes

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util"
	jahresabschluss "gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/jahresabschluss/jahresabschlussrechner"
)

const HHJ_QUERY = "haushaltsjahr"
const ID_PARAM = "anlageId"

func RegisterJahresabschlussRoutes(group *gin.RouterGroup) {
	group.GET("/:"+ID_PARAM+"/", getJahresabschlussForAnlageIdForhaushaltsjahr)
	group.GET("/:"+ID_PARAM+"/download", getJahresAbschlussDocuments)
}

func getJahresabschlussForAnlageIdForhaushaltsjahr(c *gin.Context) {
	anlageId, err := strconv.ParseInt(c.Param(ID_PARAM), 10, 64)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	if _, err := model.GetGeldanlage(int(anlageId)); err != nil {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("no geldanlage for this id"))
		return
	}
	haushaltsjahr := c.Query(HHJ_QUERY)
	if haushaltsjahr == "" {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("query must contain haushaltsjahr"))
		return
	}
	if strings.Count(haushaltsjahr, "/") != 1 {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("wrong format for haushaltsjahr: must contain one '/'"))
		return
	}

	found_hhj := model.HasHaushaltsjahr(haushaltsjahr)
	if !found_hhj {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("haushaltsjahr not found"))
		return
	}

	jahresabschl := jahresabschlussrechner.GetJahresabschlussForHaushaltsjahrForAnlageId(haushaltsjahr, int(anlageId))
	if c.GetHeader(constants.HEADER_ACCEPT) == "application/json" {
		c.JSON(http.StatusOK, jahresabschl)
	} else {
		data, err := jahresabschlussrechner.GetJahresabschlussHtmlData(jahresabschl)
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		c.HTML(http.StatusOK, "jahresabschluss.go.tmpl", data)
	}

}

func getJahresAbschlussDocuments(c *gin.Context) {
	// Retrieve Headers
	accept_header := c.GetHeader(constants.HEADER_ACCEPT)
	accepted_types := strings.Split(accept_header, ",")

	// Retrieve and check parameters and query params
	id, err := strconv.ParseInt(c.Param(ID_PARAM), 10, 64)
	if err != nil {
		fmt.Println(err)
		c.AbortWithError(http.StatusBadRequest, err)
	}

	hhj := c.Query(HHJ_QUERY)
	if hhj == "" {
		fmt.Println(fmt.Errorf("query must contain haushaltsjahr"))
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("query must contain haushaltsjahr"))
		return
	}
	if !model.HasHaushaltsjahr(hhj) {
		err := fmt.Errorf("haushaltsjahr not found")
		fmt.Println(err)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	// Get the filepath for either the pdf of the zip file while creating or modifying them to be up to date
	if util.Contains(accepted_types, constants.HEADER_PDF, constants.HEADER_ALL) {
		path, err := jahresabschluss.GetOrCreateJahresabschlussPDF(hhj, id)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}
		c.FileFromFS(path, constants.PDF_JABSCHL_FS)
	} else if util.Contains(accepted_types, constants.HEADER_APPL_ZIP) {
		filename_all_zip, err := jahresabschluss.GetOrCreateJahresabschlussZIP(hhj, id)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}
		c.FileFromFS(filename_all_zip, constants.ZIP_FS)
	} else {
		c.AbortWithStatus(http.StatusUnsupportedMediaType)
		return
	}
}
