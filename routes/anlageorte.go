package routes

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/copier"
	"gitlab.com/matthias-eb/fs-kassenorganizer/model"
	structs "gitlab.com/matthias-eb/fs-kassenorganizer/routes/structs"
)

func RegisterAnlageortHandlers(group *gin.RouterGroup) {
	group.GET("/", getAllGeldanlagen)
	group.GET("/:anlageId/", getAnlageort)
	group.GET("/:anlageId/kassenpruefungen", getAnlageortKassenpruefungen)
}

func getAllGeldanlagen(c *gin.Context) {
	geldanlagen := model.GetGeldanlagen()
	c.JSON(http.StatusOK, geldanlagen)
}

func getAnlageort(c *gin.Context) {
	anlageId, err := strconv.ParseInt(c.Param("anlageId"), 10, 64)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	geldanlage, err := model.GetGeldanlage((int)(anlageId))
	if err != nil {
		c.AbortWithError(http.StatusNotFound, err)
	} else {
		c.JSON(http.StatusOK, geldanlage)
	}
}

func getAnlageortKassenpruefungen(c *gin.Context) {
	kpforAnlageort := structs.Anlageort_Kassenpruefungen{}
	anlageort, err := model.GetGeldanlage(1)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	err = copier.Copy(&kpforAnlageort, &anlageort)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	kassenpruefungen, err := model.GetKassenpruefungenForAnlageort(1)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	err = copier.Copy(&kpforAnlageort.Kassenpruefungen, &kassenpruefungen)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, kpforAnlageort)
}
