package routes

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	cst "gitlab.com/matthias-eb/fs-kassenorganizer/constants"
	mdl "gitlab.com/matthias-eb/fs-kassenorganizer/model"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/pdf"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/pdf_filler"
	"gitlab.com/matthias-eb/fs-kassenorganizer/util/util_zip"

	"github.com/gin-gonic/gin"
)

// @BasePath /kassenanweisungen

const ALL_ZIP_NAME = "all.zip"
const ALL_JSON_NAME = "all.json"

var PATH_ALL_ZIP = cst.ZIP_FOLDER + ALL_ZIP_NAME
var PATH_ALL_JSON = cst.JSON_FOLDER + ALL_JSON_NAME

func RegisterKassenanweisungHandlers(group *gin.RouterGroup) {
	group.GET("/", getAllKassenanweisungen)
	group.GET("/:id", getKassenanweisung)
	group.GET("/:id/:hhj", getKassenanweisungenForHaushaltsjahr)
	group.GET("/multiple", getMultipleKassenanweisungen)
}

// @Produce json,pdf,zip
// @Success 200
// @Router / [get]
func getAllKassenanweisungen(c *gin.Context) {
	kassenanweisungen := mdl.GetAllKassenanweisungen()
	accepted := strings.Split(c.GetHeader(cst.HEADER_ACCEPT), ",")
	if util.Contains(accepted, cst.HEADER_APPL_JSON) {
		c.JSON(http.StatusOK, kassenanweisungen)
	} else {

		// Check if the ZIP file already exists, and if so, if the JSON exists as well
		_, err1 := os.Stat(PATH_ALL_JSON)
		_, err2 := os.Stat(PATH_ALL_ZIP)

		if os.IsNotExist(err1) || os.IsNotExist(err2) {

			file_paths := pdf_filler.KassenanweisungenToPDF(kassenanweisungen)
			util_zip.CreateZipAndJsonFile(file_paths, kassenanweisungen, ALL_ZIP_NAME, ALL_JSON_NAME)

		} else {
			util_zip.AddMissingKassenanweisungenToZip(kassenanweisungen, ALL_ZIP_NAME, ALL_JSON_NAME)
		}

		// Serve the zip file
		c.FileFromFS(ALL_ZIP_NAME, cst.ZIP_FS)
	}
}

// @Produce json,pdf,zip
// @Success 200
// @Router /:id [get]
func getKassenanweisungenForHaushaltsjahr(c *gin.Context) {
	id1, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	hhj := c.Param("hhj")
	if len(hhj) != 5 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	n1 := hhj[0:2]
	n2 := hhj[3:5]

	filename := fmt.Sprintf("%s%d-%s", cst.ID_HHJ_NAME_PREFIX, id1, n1+"_"+n2)

	// Get kassenanweisung array from database
	kassenanweisungen := mdl.QueryKassenanweisungenForHaushaltsjahrForGeldanlageOrdered(n1+"/"+n2, int(id1), "Titelnr ASC")
	if len(kassenanweisungen) == 0 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	serveAcceptedFile(c, filename, kassenanweisungen)
}

func getMultipleKassenanweisungen(c *gin.Context) {
	strid, hasQuery := c.GetQuery("ids")
	if !hasQuery {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	strids := strings.Split(strid, ",")
	ids := []int{}
	for _, s := range strids {
		id, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			fmt.Println(err)
			continue
		}
		ids = append(ids, int(id))
	}

	intArrhash := util.ComputeHashForIntList(ids, ",")

	kassenanweisungen, err := mdl.GetMultipleKassenanweisung(ids)
	if err != nil {
		fmt.Println(fmt.Errorf("unable to get Kassenanweisungen for Ids: %w", err))
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if len(kassenanweisungen) != len(ids) {
		fmt.Println(kassenanweisungen)
		err = errors.New("anzahl der Kassenanweisungen stimmt nicht mit der Azahl der Ids überein")
		fmt.Println(err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	serveAcceptedFile(c, intArrhash, kassenanweisungen)
}

func getKassenanweisung(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	kassenanweisung, err := mdl.GetKassenanweisung(int(id))
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	if c.GetHeader(cst.HEADER_ACCEPT) == cst.HEADER_APPL_JSON {
		c.JSON(http.StatusOK, kassenanweisung)
	} else {
		kassenanweisungen := append([]mdl.Kassenanweisung_model{}, kassenanweisung)

		filepaths := pdf_filler.KassenanweisungenToPDF(kassenanweisungen)
		if len(filepaths) != 1 {
			panic(errors.New("wrong length for filepaths"))
		}
		c.File(filepaths[0])
	}
}

// serveAcceptedFile consolidates the handling of serving the wanted file type into one method.
func serveAcceptedFile(c *gin.Context, filename string, kassenanweisungen []mdl.Kassenanweisung_model) {
	var filename_zip = fmt.Sprintf("%s.zip", filename)
	var filename_json = fmt.Sprintf("%s.json", filename)
	var filename_pdf = fmt.Sprintf("%s.pdf", filename)

	accepted := strings.Split(c.GetHeader("Accept"), ",")
	// Send answer according to Accept Header
	if util.Contains(accepted, cst.HEADER_APPL_JSON) {
		c.JSON(http.StatusOK, kassenanweisungen)
	} else if util.Contains(accepted, cst.HEADER_ALL, cst.HEADER_APPL_ZIP) {

		// Check if the ZIP file already exists, and if so, if the JSON exists as well
		_, err1 := os.Stat(cst.JSON_FOLDER + filename_json)
		_, err2 := os.Stat(cst.ZIP_FOLDER + filename_zip)

		if os.IsNotExist(err1) || os.IsNotExist(err2) {
			if os.IsNotExist(err1) {
				util.CreateDirIfNotExists(cst.JSON_FOLDER)
			} else {
				util.CreateDirIfNotExists(cst.ZIP_FOLDER)
			}

			file_paths := pdf_filler.KassenanweisungenToPDF(kassenanweisungen)
			util_zip.CreateZipAndJsonFile(file_paths, kassenanweisungen, filename_zip, filename_json)

		} else {
			util_zip.AddMissingKassenanweisungenToZip(kassenanweisungen, filename_zip, filename_json)
		}

		// Serve the zip file
		c.FileFromFS(filename_zip, cst.ZIP_FS)
	} else if util.Contains(accepted, cst.HEADER_PDF) {
		_, err := pdf.CreateAndMergePDFs(kassenanweisungen, filename)
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
		}

		c.FileFromFS(filename_pdf, cst.PDF_MERGE_FS)
	} else {
		c.AbortWithStatus(http.StatusUnsupportedMediaType)
	}
}
