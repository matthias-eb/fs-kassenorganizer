module gitlab.com/matthias-eb/fs-kassenorganizer

go 1.16

require github.com/joho/godotenv v1.3.0

require (
	bitbucket.org/inceptionlib/pdfinject-go/pdfinject v0.0.0-20190926132212-5c8fcb189f80
	github.com/SebastiaanKlippert/go-wkhtmltopdf v1.7.2
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/gin-gonic/gin v1.7.4
	github.com/go-openapi/spec v0.20.6 // indirect
	github.com/go-openapi/swag v0.21.1 // indirect
	github.com/jinzhu/copier v0.3.2
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pdfcpu/pdfcpu v0.3.13 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2 // indirect
	github.com/swaggo/swag v1.8.1 // indirect
	github.com/urfave/cli/v2 v2.6.0 // indirect
	golang.org/x/net v0.0.0-20220425223048-2871e0cb64e4 // indirect
	golang.org/x/sys v0.0.0-20220503163025-988cb79eb6c6 // indirect
	golang.org/x/text v0.3.7
	golang.org/x/tools v0.1.10 // indirect
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.22.2
	moul.io/number-to-words v0.7.0
)
