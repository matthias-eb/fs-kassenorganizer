# FS-Kassenorganizer

Ein Programm zum füllen von PDF Form Kassenanweisungen mit den Daten aus einer SQL-Datenbank.

## Description

This program is meant for filling out PDF Forms for payment orders (Kassenanweisungen) automatically. It is further used to create the annual account overview that is given to the ASTA at the end of each financial year.

## Installation

To download the program, first execute the following code in a shell: 
```bash
git clone https://gitlab.com/matthias-eb/fs-kassenorganizer.git
cd fs-kassenorganizer
```

### Installing mariadb docker container

Before starting the docker container, the following needs to be done:
- Move all sql files with data that should be put into the docker container into `docker_db/init/`. It will (probably) be executed on first launch.
- create the directory `docker_db/data/`. It will be the place where the mysql data is kept and prevent the loss of data when the docker container is removed.
- If you want to move the folder for the sql files or the data elsewhere, do so and edit the `docker-compose.yml` file under the point 
```yml
services:
    fs-kassenorganizer-db:
        volumes:
```
- You can change the password of the root and fachschaft user to something else in the `docker-compose.yml` file as well. Remember to change the .env files of the frontend and backend as well though.

To create the database docker container, execute:
`docker-compose up -d fs-kassenorganizer-db`

This will create a docker container with mariadb and a Database Fachschaft_ET set up.


### First initialization
- Access mariadb in the docker container
`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' fs-kassenorganizer-db`
`mysql -h <ip> -P 3306 --protocol=TCP -u fachschaft -p`

- Check if the tables exist
```sql
USE Fachschaft_ET;
SHOW tables;
```
- if the tables don't exist, exit mariadb with `exit` and execute the sql files in the docker container:
    - attach shell to docker container and execute sql files:
```bash
docker exec -it fs-kassenorganizer-db bash
mariadb -u fachschaft -p < /docker-entrypoint-initdb.d/0_init_db.sql # create the tables
mariadb -u fachschaft -p < /docker-entrypoint-initdb.d/1_data_export.sql # populate data
```


### Executing the program from a docker container

#### Preparations

If we execute the program in a docker container, we need to create a docker.env file. To do that, please copy the `.env.sample` file and rename it to `docker.env`. Then, fill in the values.
You need too have started the database docker container beforehand to be able to fill out the IP Address field. You can show the ip address with the command `docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' fs-kassenorganizer-db`

Create a folder on the host of the machine that the container will run on at `~/fs-kassenorganizer/db/data`.

The execution works with both docker-compose and docker, Although I suggest the use of docker-compose, since it is easier to do.
This program features both a command-line program as well as a Restful API. The Command-line program ist currently not maintained. I strongly suggest you use the RESTful API together with the frontend.

#### Executing the Console Program

For executing the console program, simply execute the following:
```
docker-compose up fs-kassenorganizer-console
```

#### Executing the RESTful API

The RESTful API is executed vie the following command:
```
docker-compose up fs-kassenorganizer --build
```

### Executing the program from the console

To execute the program in the console directly, first build the program:

```
go build .
```
Then, execute it as a RESTful API with the following command:

```bash
./fs-kassenorganizer
```
Or use the command line program with the following command:
```bash
./fs-kassenorganizer -C
```

### Frontend-Project made by Patrick Schmidt

To create the docker container of patrick schmidt's frontend, execute the following:
```bash
docker-compose up -d fs-kassenorganizer-frontend
```
To update the container, execute
```bash
docker-compose build --no-cache fs-kassenorganizer-frontend
docker-compose up -d fs-kassenorganizer-frontend
```

### Updating the Docker-Containers

To update one or more containers, first stop the docker container:
```bash
docker-compose down fs-kassenorganizer
```
Then remove all lingering images:
```bash
docker image rm fs-kassenorganizer_fs-kassenorganizer
```
Lastly, bring the images up again
```bash
docker-compose up -d
```

## Removing Docker containers individually

To remove a single docker container, execute the following (here with the example of the frontend):
```bash
docker container stop fs-kassenorganizer-fs-kassenorganizer-frontend-1 && docker container rm fs-kassenorganizer-fs-kassenorganizer-frontend-1
```

To remove the whole stack, execute
```bash
docker-compose down
```

Above methods will not affect the volumes that are attached to the database server. All tables and data will be saved and can be accessed on the next start.

## Support

For support, email me or create an Issue on the gitlab page.

## Roadmap

The planned functions of this program are listed here:

### POST Method '/kassenpruefungen'

cash audits should be posted through this api. If the amount does not match the calculated one, the amount that needs to be filled is returned together with all account statements. 

### POST Method '/kassenanweisungen'

The kassenanweisungen shouldn't be added through a database editor anymore but through this api, as there are quite a few traps when creating one, which would make the annual account overview run into errors, specifically:
- Create two Kassenanweisungen when moving money from the beverage cass to the safe, one for adding the amount to the beverage cass with the origin of unknown, and the actual one. This function is needed until a proper cass system exists which monitors the sold beverages and their price.
- Create missing accounts or owners when they are not found

### Decipher account statements of the Sparkasse with the help of OCR (Optical Character Recognition) to automatically create cash audits

The statements of accounts from the Sparkasse don't use text fields, but rather Vectors. For this reason, we need OCR. 
A library that can be used for this would be [gosseract](https://github.com/otiai10/gosseract). The exported values should be used to create audits and to identify missing payment orders.

### Export the annual account overview as PDF

The annual accout overview should be generated automatically and be exported as PDF. This can be done by using HTML Templates and then exporting it to PDF.

## Contributing

Contributions are always welcome if it provides useful functionality. Please note that the changes should be well documented and described.
Contributions can be done through Merge requests.
Contributors will be listed here in the README.md file.

## Contributors

There are currently no contributors.

## License
All source code in this project is licensed under the GPL-3.0 License.

## Project status
The project is currently under active development.